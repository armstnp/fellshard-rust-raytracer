use crate::color::Color;
use crate::tuple::{vec, Tuple};
use crate::matrix::M44;
use crate::shape::Shape;

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Blend {
    Linear
}

impl Blend {
    fn blend(&self, color_a: Color, color_b: Color, ratio: f64) -> Color {
        let distance = color_b - color_a;

        color_a + distance * ratio
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Pattern {
    t: PatternType,
    transform: M44
}

impl Pattern {
    pub fn set_transform(&mut self, transform: M44) {
        self.transform = transform;
    }

    pub fn with_transform(self, transform: M44) -> Self {
        Pattern { t: self.t, transform: transform }
    }

    pub fn pattern_at_object(&self, object: &dyn Shape, world_point: Tuple) -> Color {
        let object_point = object.transform().inverse() * world_point;

        self.pattern_at_object_point(object_point)
    }

    fn pattern_at_object_point(&self, object_point: Tuple) -> Color {
        let pattern_point = self.transform.inverse() * object_point;

        self.t.pattern_at(pattern_point)
    }
}

#[derive(Debug, Clone, PartialEq)]
enum PatternType {
    Space,
    Solid(Color),
    Perturb { pat: Box<Pattern>, noises: PerlinWrapper, extent: f64 },
    Stripe(Box<Pattern>, Box<Pattern>),
    Gradient(Box<Pattern>, Box<Pattern>, Blend),
    PingPongGradient(Box<Pattern>, Box<Pattern>, Blend),
    RadialGradient(Box<Pattern>, Box<Pattern>, Blend),
    PingPongRadialGradient(Box<Pattern>, Box<Pattern>, Blend),
    Ring(Box<Pattern>, Box<Pattern>),
    Checkers(Box<Pattern>, Box<Pattern>),
    Blended(Box<Pattern>, Box<Pattern>, Blend, f64)
}

impl PatternType {
    fn pattern_at(&self, point: Tuple) -> Color {
        use PatternType::*;
        use crate::color::color;

        match self {
            Space => color(point.x, point.y, point.z),
            Solid(color) => *color,
            Perturb { .. } => self.perturb_at(point),
            Stripe(..) => self.stripe_at(point),
            Gradient(..) => self.gradient_at(point),
            PingPongGradient(..) => self.ping_pong_gradient_at(point),
            RadialGradient(..) => self.radial_gradient_at(point),
            PingPongRadialGradient(..) => self.ping_pong_radial_gradient_at(point),
            Ring(..) => self.ring_at(point),
            Checkers(..) => self.checkers_at(point),
            Blended(..) => self.blended_at(point)
        }
    }

    fn perturb_at(&self, point: Tuple) -> Color {
        if let PatternType::Perturb { pat, noises, extent } = self {
            let jitter = noises.get(point);
            let jittered = point + *extent * jitter;
            pat.pattern_at_object_point(jittered)
        } else {
            panic!("Not a perturb")
        }
    }

    fn stripe_at(&self, point: Tuple) -> Color {
        if let PatternType::Stripe(a, b) = self {
            if point.x.floor() as i64 % 2 == 0 {
                a
            } else {
                b
            }.pattern_at_object_point(point)
        } else {
            panic!("Not a stripe")
        }
    }

    fn gradient_at(&self, point: Tuple) -> Color {
        if let PatternType::Gradient(a, b, blend) = self {
            let a_color = a.pattern_at_object_point(point);
            let b_color = b.pattern_at_object_point(point);
            let fraction = point.x - point.x.floor();

            blend.blend(a_color, b_color, fraction)
        } else {
            panic!("Not a gradient")
        }
    }

    fn ping_pong_gradient_at(&self, point: Tuple) -> Color {
        if let PatternType::PingPongGradient(a, b, blend) = self {
            let a_color = a.pattern_at_object_point(point);
            let b_color = b.pattern_at_object_point(point);
            let mut fraction = point.x - point.x.floor();

            fraction = if point.x.floor() as i64 % 2 == 0 {
                fraction
            } else {
                1. - fraction
            };

            blend.blend(a_color, b_color, fraction)
        } else {
            panic!("Not a ping-pong gradient")
        }
    }

    fn radial_gradient_at(&self, point: Tuple) -> Color {
        if let PatternType::RadialGradient(a, b, blend) = self {
            let a_color = a.pattern_at_object_point(point);
            let b_color = b.pattern_at_object_point(point);
            let radius = (point.x * point.x + point.z * point.z).sqrt();
            let fraction = radius - radius.floor();

            blend.blend(a_color, b_color, fraction)
        } else {
            panic!("Not a radial gradient")
        }
    }

    fn ping_pong_radial_gradient_at(&self, point: Tuple) -> Color {
        if let PatternType::PingPongRadialGradient(a, b, blend) = self {
            let a_color = a.pattern_at_object_point(point);
            let b_color = b.pattern_at_object_point(point);
            let radius = (point.x * point.x + point.z * point.z).sqrt();
            let mut fraction = radius - radius.floor();

            fraction = if radius.floor() as i64 % 2 == 0 {
                fraction
            } else {
                1. - fraction
            };

            blend.blend(a_color, b_color, fraction)
        } else {
            panic!("Not a ping-pong radial gradient")
        }
    }

    fn ring_at(&self, point: Tuple) -> Color {
        if let PatternType::Ring(a, b) = self {
            if (point.x * point.x + point.z * point.z).sqrt().floor() as u64 % 2 == 0 {
                a
            } else {
                b
            }.pattern_at_object_point(point)
        } else {
            panic!("Not a ring")
        }
    }

    fn checkers_at(&self, point: Tuple) -> Color {
        if let PatternType::Checkers(a, b) = self {
            if (point.x.floor() + point.y.floor() + point.z.floor()) as i64 % 2 == 0 {
                a
            } else {
                b
            }.pattern_at_object_point(point)
        } else {
            panic!("Not checkers")
        }
    }

    fn blended_at(&self, point: Tuple) -> Color {
        if let PatternType::Blended(a, b, blend, mix) = self {
            let a_color = a.pattern_at_object_point(point);
            let b_color = b.pattern_at_object_point(point);
            blend.blend(a_color, b_color, *mix)
        } else {
            panic!("Not blended")
        }
    }
}

pub fn solid(color: Color) -> Pattern {
    pattern(PatternType::Solid(color))
}

pub fn perturb(pat: Pattern, extent: f64) -> Pattern {
    use noise::{Perlin, Seedable};

    let p1 = Perlin::new();
    let p2 = Perlin::new().set_seed(1);
    let p3 = Perlin::new().set_seed(2);
    let noises = PerlinWrapper(p1, p2, p3);

    pattern(PatternType::Perturb { pat: Box::new(pat), noises: noises, extent: extent })
}

pub fn stripe(pattern_a: Pattern, pattern_b: Pattern) -> Pattern {
    pattern(PatternType::Stripe(Box::new(pattern_a), Box::new(pattern_b)))
}

pub fn gradient(pattern_a: Pattern, pattern_b: Pattern, blend: Blend) -> Pattern {
    pattern(PatternType::Gradient(Box::new(pattern_a), Box::new(pattern_b), blend))
}

pub fn ping_pong_gradient(pattern_a: Pattern, pattern_b: Pattern, blend: Blend) -> Pattern {
    pattern(PatternType::PingPongGradient(Box::new(pattern_a), Box::new(pattern_b), blend))
}

pub fn radial_gradient(pattern_a: Pattern, pattern_b: Pattern, blend: Blend) -> Pattern {
    pattern(PatternType::RadialGradient(Box::new(pattern_a), Box::new(pattern_b), blend))
}

pub fn ping_pong_radial_gradient(pattern_a: Pattern, pattern_b: Pattern, blend: Blend) -> Pattern {
    pattern(PatternType::PingPongRadialGradient(Box::new(pattern_a), Box::new(pattern_b), blend))
}

pub fn ring(pattern_a: Pattern, pattern_b: Pattern) -> Pattern {
    pattern(PatternType::Ring(Box::new(pattern_a), Box::new(pattern_b)))
}

pub fn checkers(pattern_a: Pattern, pattern_b: Pattern) -> Pattern {
    pattern(PatternType::Checkers(Box::new(pattern_a), Box::new(pattern_b)))
}

pub fn blended(pattern_a: Pattern, pattern_b: Pattern, blend: Blend, mix: f64) -> Pattern {
    pattern(PatternType::Blended(Box::new(pattern_a), Box::new(pattern_b), blend, mix))
}

pub fn space() -> Pattern {
    pattern(PatternType::Space)
}

fn pattern(t: PatternType) -> Pattern {
    Pattern { t: t, transform: M44::IDENTITY }
}

#[derive(Debug, Clone)]
struct PerlinWrapper(noise::Perlin, noise::Perlin, noise::Perlin);

impl PartialEq for PerlinWrapper {
    fn eq(&self, other: &Self) -> bool {
        std::ptr::eq(self, other)
    }
}

impl PerlinWrapper {
    fn get(&self, source_point: Tuple) -> Tuple {
        use noise::NoiseFn;

        let p3: noise::Point3<f64> = source_point.into();

        vec(self.0.get(p3), self.1.get(p3), self.2.get(p3))
    }
}

#[cfg(test)]
pub mod test {
    use crate::pattern::*;
    use crate::pattern::Blend::Linear;
    use crate::tuple::point;
    use crate::color::{color, Color};
    use crate::matrix::{scaling, translation};
    use crate::sphere::sphere;

    static WHITE: Color = Color::WHITE;
    static BLACK: Color = Color::BLACK;

    pub fn test_pattern() -> Pattern {
        space()
    }

    #[test]
    fn creating_a_stripe_pattern() {
        let s = stripe(solid(WHITE), solid(BLACK));
        match s.t {
            PatternType::Stripe(a, b) => {
                assert_eq!(*a, solid(WHITE));
                assert_eq!(*b, solid(BLACK));
            },
            _ => panic!("Not a stripe")
        }
    }

    #[test]
    fn a_stripe_pattern_is_constant_in_y() {
        let s = stripe(solid(WHITE), solid(BLACK)).t;
        assert_eq!(s.pattern_at(Tuple::ORIGIN), WHITE);
        assert_eq!(s.pattern_at(point(0, 1, 0)), WHITE);
        assert_eq!(s.pattern_at(point(0, 2, 0)), WHITE);
    }

    #[test]
    fn a_stripe_pattern_is_constant_in_z() {
        let s = stripe(solid(WHITE), solid(BLACK)).t;
        assert_eq!(s.pattern_at(Tuple::ORIGIN), WHITE);
        assert_eq!(s.pattern_at(point(0, 0, 1)), WHITE);
        assert_eq!(s.pattern_at(point(0, 0, 2)), WHITE);
    }

    #[test]
    fn a_stripe_pattern_alternates_in_x() {
        let s = stripe(solid(WHITE), solid(BLACK)).t;
        assert_eq!(s.pattern_at(Tuple::ORIGIN), WHITE);
        assert_eq!(s.pattern_at(point(0.9, 0, 0)), WHITE);
        assert_eq!(s.pattern_at(point(1, 0, 0)), BLACK);
        assert_eq!(s.pattern_at(point(-0.1, 0, 0)), BLACK);
        assert_eq!(s.pattern_at(point(-1, 0, 0)), BLACK);
        assert_eq!(s.pattern_at(point(-1.1, 0, 0)), WHITE);
    }

    #[test]
    fn the_default_pattern_transformation() {
        let pattern = solid(WHITE);
        assert_eq!(pattern.transform, M44::IDENTITY);
    }

    #[test]
    fn assigning_a_transformation() {
        let mut pattern = solid(WHITE);
        pattern.set_transform(translation(1, 2, 3));
        assert_eq!(pattern.transform, translation(1, 2, 3));
    }

    #[test]
    fn stripes_with_an_object_transformation() {
        let mut object = sphere();
        object.set_transform(scaling(2, 2, 2));

        let pattern = test_pattern();
        let c = pattern.pattern_at_object(&object, point(2, 3, 4));
        assert_eq!(c, color(1, 1.5, 2));
    }

    #[test]
    fn stripes_with_a_pattern_transformation() {
        let object = sphere();

        let mut pattern = test_pattern();
        pattern.set_transform(scaling(2, 2, 2));

        let c = pattern.pattern_at_object(&object, point(2, 3, 4));
        assert_eq!(c, color(1, 1.5, 2));
    }

    #[test]
    fn stripes_with_both_an_object_and_a_pattern_transformation() {
        let mut object = sphere();
        object.set_transform(scaling(2, 2, 2));

        let mut pattern = test_pattern();
        pattern.set_transform(translation(0.5, 1, 1.5));

        let c = pattern.pattern_at_object(&object, point(2.5, 3, 3.5));
        assert_eq!(c, color(0.75, 0.5, 0.25));
    }

    #[test]
    fn a_gradient_linearly_interpolates_between_colors() {
        let g = gradient(solid(WHITE), solid(BLACK), Linear).t;
        assert_eq!(g.pattern_at(Tuple::ORIGIN), WHITE);
        assert_eq!(g.pattern_at(point(0.25, 0, 0)), WHITE * 0.75);
        assert_eq!(g.pattern_at(point(0.5, 0, 0)), WHITE * 0.5);
        assert_eq!(g.pattern_at(point(0.75, 0, 0)), WHITE * 0.25);
    }

    #[test]
    fn a_ring_should_extend_in_both_x_and_z() {
        let r = ring(solid(WHITE), solid(BLACK)).t;
        assert_eq!(r.pattern_at(Tuple::ORIGIN), WHITE);
        assert_eq!(r.pattern_at(point(1, 0, 0)), BLACK);
        assert_eq!(r.pattern_at(point(0, 0, 1)), BLACK);
        // 0.708 = just slightly more than √2/2
        assert_eq!(r.pattern_at(point(0.708, 0, 0.708)), BLACK);
    }

    #[test]
    fn checkers_should_repeat_in_x() {
        let c = checkers(solid(WHITE), solid(BLACK)).t;
        assert_eq!(c.pattern_at(Tuple::ORIGIN), WHITE);
        assert_eq!(c.pattern_at(point(0.99, 0, 0)), WHITE);
        assert_eq!(c.pattern_at(point(1.01, 0, 0)), BLACK);
    }

    #[test]
    fn checkers_should_repeat_in_y() {
        let c = checkers(solid(WHITE), solid(BLACK)).t;
        assert_eq!(c.pattern_at(Tuple::ORIGIN), WHITE);
        assert_eq!(c.pattern_at(point(0, 0.99, 0)), WHITE);
        assert_eq!(c.pattern_at(point(0, 1.01, 0)), BLACK);
    }

    #[test]
    fn checkers_should_repeat_in_z() {
        let c = checkers(solid(WHITE), solid(BLACK)).t;
        assert_eq!(c.pattern_at(Tuple::ORIGIN), WHITE);
        assert_eq!(c.pattern_at(point(0, 0, 0.99)), WHITE);
        assert_eq!(c.pattern_at(point(0, 0, 1.01)), BLACK);
    }
}
