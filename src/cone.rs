use crate::tuple::{vec, Tuple};
use crate::matrix::M44;
use crate::material::Material;
use crate::shape::{base_shape, Shape, BaseShape};
use crate::ray::Ray;
use crate::intersection::{intersection, intersections, Intersection, Intersections};
use delegate::delegate;

#[derive(Debug)]
pub struct Cone {
    base: BaseShape,
    pub minimum: f64,
    pub maximum: f64,
    pub closed: bool
}

impl Cone {
    const EPSILON: f64 = 1e-7;

    fn check_cap(cap_y: f64, ray: &Ray, t: f64) -> bool {
        let Tuple { x: orig_x, z: orig_z, .. } = ray.origin;
        let Tuple { x: dir_x, z: dir_z, .. } = ray.direction;

        let x = orig_x + t * dir_x;
        let z = orig_z + t * dir_z;

        x * x + z * z <= cap_y * cap_y
    }

    fn intersect_caps<'a>(&'a self, ray: &Ray, ixs: &mut Vec<Intersection<'a>>) {
        let Tuple { y: orig_y, .. } = ray.origin;
        let Tuple { y: dir_y, .. } = ray.direction;

        if !self.closed || dir_y.abs() <= Self::EPSILON {
            return
        }

        if self.minimum.is_finite() {
            let t = (self.minimum - orig_y) / dir_y;
            if Self::check_cap(self.minimum, ray, t) {
                ixs.push(intersection(t, self));
            }
        }

        if self.maximum.is_finite() {
            let t = (self.maximum - orig_y) / dir_y;
            if Self::check_cap(self.maximum, ray, t) {
                ixs.push(intersection(t, self));
            }
        }
    }
}

impl Shape for Cone {
    delegate! {
        to self.base {
            fn transform(&self) -> &M44;
            fn set_transform(&mut self, transform: M44);
            fn material(&self) -> &Material;
            fn material_mut(&mut self) -> &mut Material;
            fn set_material(&mut self, material: Material);
        }
    }

    fn type_tag(&self) -> String { "Cone".to_string() }

    fn local_normal_at(&self, local_point: Tuple) -> Tuple {
        let Tuple { x, y, z, .. } = local_point;
        let dist = x * x + z * z;

        if dist < y * y {
            if y >= self.maximum - Self::EPSILON {
                return Tuple::Y_UNITV;
            } else if y <= self.minimum + Self::EPSILON {
                return -Tuple::Y_UNITV;
            }
        }

        let mut norm_y = (x * x + z * z).sqrt();
        if y > 0. {
            norm_y = -norm_y;
        }

        vec(x, norm_y, z)
    }

    fn local_intersect(&self, local_ray: &Ray) -> Intersections {
        use std::mem;

        let Tuple { x: orig_x, y: orig_y, z: orig_z, .. } = local_ray.origin;
        let Tuple { x: dir_x, y: dir_y, z: dir_z, .. } = local_ray.direction;
        let mut ixs = vec![];

        let a = dir_x * dir_x - dir_y * dir_y + dir_z * dir_z;
        let b = 2. * (orig_x * dir_x - orig_y * dir_y + orig_z * dir_z);
        if a.abs() > Self::EPSILON {
            let c = orig_x * orig_x - orig_y * orig_y + orig_z * orig_z;
            let disc = b * b - 4. * a * c;

            if disc < 0. {
                return intersections(vec![]);
            }

            let disc_root = disc.sqrt();
            let mut t0 = (- b - disc_root) / (2. * a);
            let mut t1 = (- b + disc_root) / (2. * a);
            if t0 > t1 {
                mem::swap(&mut t0, &mut t1);
            }

            let y0 = orig_y + t0 * dir_y;
            if self.minimum < y0 && y0 < self.maximum {
                ixs.push(intersection(t0, self));
            }

            let y1 = orig_y + t1 * dir_y;
            if self.minimum < y1 && y1 < self.maximum {
                ixs.push(intersection(t1, self));
            }
        } else if b.abs() > Self::EPSILON {
            let c = orig_x * orig_x - orig_y * orig_y + orig_z * orig_z;
            let t = - c / (2. * b);

            let y = orig_y + t * dir_y;
            if self.minimum < y && y < self.maximum {
                ixs.push(intersection(t, self));
            }
        }

        self.intersect_caps(local_ray, &mut ixs);

        intersections(ixs)
    }
}

pub fn cone() -> Cone {
    Cone {
        base: base_shape(),
        minimum: f64::NEG_INFINITY,
        maximum: f64::INFINITY,
        closed: false
    }
}

#[cfg(test)]
mod test {
    use crate::cone::*;
    use crate::tuple::{point, vec};
    use crate::ray::ray;
    use approx::*;

    #[test]
    fn intersecting_a_cone_with_a_ray() {
        let shape = cone();

        let cases = [
            (point(0, 0, -5), Tuple::Z_UNITV  , 5.,       5.     ),
            (point(0, 0, -5), vec( 1  ,  1, 1), 8.66025,  8.66025),
            (point(1, 1, -5), vec(-0.5, -1, 1), 4.55006, 49.44994),
        ];

        for (i, (origin, direction, t0, t1)) in cases.iter().enumerate() {
            let r = ray(*origin, direction.norm());
            let ixs = shape.local_intersect(&r);

            assert_eq!(ixs.len(), 2, "Case {} - Expected 2 intersections, got {}", i, ixs.len());
            assert!(
                abs_diff_eq!(ixs[0].t, *t0, epsilon = 1e-5),
                "Case {} - Expected t0 to be {}, got {}", i, *t0, ixs[0].t);
            assert!(
                abs_diff_eq!(ixs[1].t, *t1, epsilon = 1e-5),
                "Case {} - Expected t1 to be {}, got {}", i, *t1, ixs[1].t);
        }
    }

    #[test]
    fn intersecting_a_cone_with_a_ray_parallel_to_one_of_its_halves() {
        let shape = cone();
        let direction = vec(0, 1, 1).norm();
        let r = ray(point(0, 0, -1), direction);
        let ixs = shape.local_intersect(&r);

        assert_eq!(ixs.len(), 1);
        assert_abs_diff_eq!(ixs[0].t, 0.35355, epsilon = 1e-5);
    }

    #[test]
    fn intersecting_a_cones_end_caps() {
        let mut shape = cone();
        shape.minimum = -0.5;
        shape.maximum = 0.5;
        shape.closed = true;

        let cases = [
            (point(0, 0, -5), Tuple::Y_UNITV, 0),
            (point(0, 0, -0.25), vec(0, 1, 1), 2),
            (point(0, 0, -0.25), Tuple::Y_UNITV, 4)
        ];

        for (i, (origin, direction, count)) in cases.iter().enumerate() {
            let r = ray(*origin, direction.norm());
            let ixs = shape.local_intersect(&r);
            assert_eq!(
                ixs.len(), *count,
                "Case {} - Expected {} intersections, got {}", i, *count, ixs.len());
        }
    }

    #[test]
    fn computing_the_normal_vector_on_a_cone() {
        let shape = cone();

        let cases = [
            (Tuple::ORIGIN, vec(0, 0, 0)),
            (point(1, 1, 1), vec(1, -(2f64.sqrt()), 1)),
            (point(-1, -1, 0), vec(-1, 1, 0))
        ];

        for (i, (p, normal)) in cases.iter().enumerate() {
            let n = shape.local_normal_at(*p);

            assert!(
                abs_diff_eq!(n, *normal),
                "Case {} - Expected normal {:?}, got {:?}", i, *normal, n);
        }
    }
}
