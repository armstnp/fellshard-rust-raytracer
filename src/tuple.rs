use approx::*;
use std::fmt;
use std::ops::{Add, Sub, Neg, Mul, Div};
use noise;

#[derive(Debug, PartialEq, Copy, Clone)]
pub struct Tuple {
    pub x: f64,
    pub y: f64,
    pub z: f64,
    pub w: f64,
}

impl approx::AbsDiffEq for Tuple {
    type Epsilon = f64;

    fn default_epsilon() -> Self::Epsilon { 1e-7 }

    fn abs_diff_eq(&self, other: &Self, epsilon: Self::Epsilon) -> bool {
        // TODO: This implementation will behave more like a relative equality...
        (self - other).mag().abs_diff_eq(&0., epsilon)
    }
}

impl Tuple {
    pub const ORIGIN: Tuple = Tuple { x: 0., y: 0., z: 0., w: 1. };
    pub const X_UNITV: Tuple = Tuple { x: 1., y: 0., z: 0., w: 0. };
    pub const Y_UNITV: Tuple = Tuple { x: 0., y: 1., z: 0., w: 0. };
    pub const Z_UNITV: Tuple = Tuple { x: 0., y: 0., z: 1., w: 0. };

    pub fn is_point(&self) -> bool {
        abs_diff_eq!(self.w, 1.0)
    }

    pub fn is_vec(&self) -> bool {
        abs_diff_eq!(self.w, 0.0)
    }

    pub fn mag(&self) -> f64 {
        if self.is_point() {
            panic!("Attempted to find the magnitude of a point: {}", self)
        }

        (self.x * self.x + self.y * self.y + self.z * self.z).sqrt()
    }

    pub fn norm(&self) -> Tuple {
        if self.is_point() {
            panic!("Attempted to normalize a point: {}", self)
        }

        self / self.mag()
    }

    pub fn cross(&self, other: Tuple) -> Tuple {
        if self.is_point() {
            panic!("Attempted to take the cross product of a point: {}", self)
        }
        if other.is_point() {
            panic!("Attempted to take the cross product of a point: {}", other)
        }

        vec(
            self.y * other.z - self.z * other.y,
            self.z * other.x - self.x * other.z,
            self.x * other.y - self.y * other.x)
    }

    pub fn reflect(&self, normal: Tuple) -> Tuple {
        self - normal * 2 * (self * normal)
    }
}

impl fmt::Display for Tuple {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut ttype = "Invalid-Tuple";
        if self.is_point() {
            ttype = "point"
        }
        if self.is_vec() {
            ttype = "vector"
        }
        write!(f, "{}({}, {}, {})", ttype, self.x, self.y, self.z)
    }
}

impl Add for Tuple {
    type Output = Tuple;

    fn add(self, other: Tuple) -> Tuple {
        &self + &other
    }
}

impl Add for &Tuple {
    type Output = Tuple;

    fn add(self, other: &Tuple) -> Tuple {
        Tuple { x: self.x + other.x, y: self.y + other.y, z: self.z + other.z, w: self.w + other.w }
    }
}

impl Add<Tuple> for &Tuple {
    type Output = Tuple;

    fn add(self, other: Tuple) -> Tuple {
        self + &other
    }
}

impl Add<&Tuple> for Tuple {
    type Output = Tuple;

    fn add(self, other: &Tuple) -> Tuple {
        &self + other
    }
}

impl Sub for Tuple {
    type Output = Tuple;

    fn sub(self, other: Tuple) -> Tuple {
        &self - &other
    }
}

impl Sub for &Tuple {
    type Output = Tuple;

    fn sub(self, other: &Tuple) -> Tuple {
        Tuple { x: self.x - other.x, y: self.y - other.y, z: self.z - other.z, w: self.w - other.w }
    }
}

impl Sub<Tuple> for &Tuple {
    type Output = Tuple;

    fn sub(self, other: Tuple) -> Tuple {
        self - &other
    }
}

impl Sub<&Tuple> for Tuple {
    type Output = Tuple;

    fn sub(self, other: &Tuple) -> Tuple {
        &self - other
    }
}

impl Neg for Tuple {
    type Output = Tuple;

    fn neg(self) -> Tuple {
        -&self
    }
}

impl Neg for &Tuple {
    type Output = Tuple;

    fn neg(self) -> Tuple {
        Tuple { x: -self.x, y: -self.y, z: -self.z, w: -self.w }
    }
}

impl<T: Into<f64>> Mul<T> for Tuple {
    type Output = Tuple;

    fn mul(self, other: T) -> Tuple {
        &self * other
    }
}

impl<T: Into<f64>> Mul<T> for &Tuple {
    type Output = Tuple;

    fn mul(self, other: T) -> Tuple {
        let o_f = other.into();
        Tuple { x: self.x * o_f, y: self.y * o_f, z: self.z * o_f, w: self.w * o_f }
    }
}

impl Mul<Tuple> for f64 {
    type Output = Tuple;

    fn mul(self, other: Tuple) -> Tuple {
        self * &other
    }
}

impl Mul<&Tuple> for f64 {
    type Output = Tuple;

    fn mul(self, other: &Tuple) -> Tuple {
        Tuple { x: other.x * self, y: other.y * self, z: other.z * self, w: other.w * self }
    }
}

impl Mul for Tuple {
    type Output = f64;

    fn mul(self, other: Tuple) -> f64 {
        &self * &other
    }
}

impl Mul<Tuple> for &Tuple {
    type Output = f64;

    fn mul(self, other: Tuple) -> f64 {
        self * &other
    }
}

impl Mul<&Tuple> for Tuple {
    type Output = f64;

    fn mul(self, other: &Tuple) -> f64 {
        &self * other
    }
}

impl Mul for &Tuple {
    type Output = f64;

    fn mul(self, other: &Tuple) -> f64 {
        if self.is_point() {
            panic!("Attempted to take the dot product of a point: {}", self)
        }
        if other.is_point() {
            panic!("Attempted to take the dot product of a point: {}", other)
        }

        self.x * other.x + self.y * other.y + self.z * other.z
    }
}

impl<T: Into<f64>> Div<T> for Tuple {
    type Output = Tuple;

    fn div(self, other: T) -> Tuple {
        &self / other
    }
}

impl<T: Into<f64>> Div<T> for &Tuple {
    type Output = Tuple;

    fn div(self, other: T) -> Tuple {
        let o_f = other.into();
        if abs_diff_eq!(o_f, 0.0) {
            panic!("Cannot divide tuple by a scale of 0.");
        }

        Tuple { x: self.x / o_f, y: self.y / o_f, z: self.z / o_f, w: self.w / o_f }
    }
}

impl From<Tuple> for [f64; 4] {
    fn from(t: Tuple) -> [f64; 4] {
        <[f64; 4]>::from(&t)
    }
}

impl From<&Tuple> for [f64; 4] {
    fn from(t: &Tuple) -> [f64; 4] {
        [t.x, t.y, t.z, t.w]
    }
}

impl From<[f64; 4]> for Tuple {
    fn from(s: [f64; 4]) -> Tuple {
        Tuple::from(&s)
    }
}

impl From<&[f64; 4]> for Tuple {
    fn from(s: &[f64; 4]) -> Tuple {
        Tuple { x: s[0], y: s[1], z: s[2], w: s[3] }
    }
}

impl From<Tuple> for noise::Point3<f64> {
    fn from(s: Tuple) -> noise::Point3<f64> {
        assert!(s.is_point(), "Cannot convert a vector to a noise-point");
        [s.x, s.y, s.z]
    }
}

impl From<noise::Point3<f64>> for Tuple {
    fn from(s: noise::Point3<f64>) -> Tuple {
        Tuple { x: s[0], y: s[1], z: s[2], w: 1. }
    }
}

// TODO: Consider +=, *=

pub fn point(x: impl Into<f64>, y: impl Into<f64>, z: impl Into<f64>) -> Tuple {
    Tuple { x: x.into(), y: y.into(), z: z.into(), w: 1. }
}

pub fn vec(x: impl Into<f64>, y: impl Into<f64>, z: impl Into<f64>) -> Tuple {
    Tuple { x: x.into(), y: y.into(), z: z.into(), w: 0. }
}

#[cfg(test)]
mod tests {
    use crate::tuple::*;

    #[test]
    fn a_tuple_with_w_eq_1_is_a_point() {
        assert!(Tuple { x: 4.3, y: -4.2, z: 3.1, w: 1. }.is_point());
        assert!(! Tuple { x: 4.3, y: -4.2, z: 3.1, w: 0. }.is_point());
    }

    #[test]
    fn a_tuple_with_w_eq_0_is_a_vector() {
        assert!(Tuple { x: 4.3, y: -4.2, z: 3.1, w: 0. }.is_vec());
        assert!(! Tuple { x: 4.3, y: -4.2, z: 3.1, w: 1. }.is_vec());
    }

    #[test]
    fn point_creates_tuples_with_w_eq_1() {
        assert!(point(4, -4, 3).is_point())
    }

    #[test]
    fn vec_creates_tuples_with_w_eq_0() {
        assert!(vec(4, -4, 3).is_vec())
    }

    #[test]
    fn identical_tuples_are_equal() {
        assert_eq!(point(4.3, 4.2, 3.1), point(4.3, 4.2, 3.1))
    }

    #[test]
    fn different_tuples_are_not_equal() {
        assert!(point(4.3, 4.2, 3.1) != point(4.2, 4.2, 3.1));
        assert!(point(4.3, 4.2, 3.1) != point(4.3, 4.1, 3.1));
        assert!(point(4.3, 4.2, 3.1) != point(4.3, 4.2, 3.0));
        assert!(point(4.3, 4.2, 3.1) != vec(4.3, 4.2, 3.1));
    }

    #[test]
    fn tuples_can_be_added() {
        assert_eq!(point(3, -2, 5) + vec(-2, 3, 1), point(1, 1, 6));
    }

    #[test]
    fn points_can_be_subtracted() {
        assert_eq!(point(3, 2, 1) - point(5, 6, 7), vec(-2, -4, -6));
    }

    #[test]
    fn vectors_can_be_subtracted() {
        assert_eq!(vec(3, 2, 1) - vec(5, 6, 7), vec(-2, -4, -6));
    }

    #[test]
    fn vectors_can_be_subtracted_from_points() {
        assert_eq!(point(3, 2, 1) - vec(5, 6, 7), point(-2, -4, -6));
    }

    #[test]
    fn tuples_can_be_negated() {
        assert_eq!(- Tuple { x: 1., y: -2., z: 3., w: -4. }, Tuple { x: -1., y: 2., z: -3., w: 4. })
    }

    #[test]
    fn tuples_can_be_scaled() {
        assert_eq!(Tuple { x: 1., y: -2., z: 3., w: -4. } * 3.5, Tuple { x: 3.5, y: -7., z: 10.5, w: -14. });
        assert_eq!(3.5 * Tuple { x: 1., y: -2., z: 3., w: -4. }, Tuple { x: 3.5, y: -7., z: 10.5, w: -14. })
    }

    #[test]
    fn tuples_can_be_scaled_by_division() {
        assert_eq!(Tuple { x: 1., y: -2., z: 3., w: -4. } / 2, Tuple { x: 0.5, y: -1., z: 1.5, w: -2. })
    }

    #[test]
    fn tuples_can_supply_their_magnitude() {
        assert_eq!(vec(1, 0, 0).mag(), 1.);
        assert_eq!(vec(0, 1, 0).mag(), 1.);
        assert_eq!(vec(0, 0, 1).mag(), 1.);
        assert_eq!(vec(1, 2, 3).mag(), 14f64.sqrt());
        assert_eq!(vec(-1, -2, -3).mag(), 14f64.sqrt());
    }

    #[test]
    fn vectors_can_normalize() {
        assert_eq!((Tuple::X_UNITV * 4).norm(), Tuple::X_UNITV);

        let sqrt14 = 14f64.sqrt();
        assert_eq!(vec(1, 2, 3).norm(), vec(1. / sqrt14, 2. / sqrt14, 3. / sqrt14));
    }

    #[test]
    fn normalized_vectors_have_length_1() {
        assert!(abs_diff_eq!(vec(1, 2, 3).norm().mag(), 1.))
    }

    #[test]
    fn vectors_can_take_their_dot_product() {
        assert!(abs_diff_eq!(vec(1, 2, 3) * vec(2, 3, 4), 20.))
    }

    #[test]
    fn vectors_can_take_their_cross_product() {
        assert_eq!(vec(1, 2, 3).cross(vec(2, 3, 4)), vec(-1, 2, -1));
        assert_eq!(vec(2, 3, 4).cross(vec(1, 2, 3)), vec(1, -2, 1));
    }

    #[test]
    fn reflecting_a_vector_approaching_at_45_deg() {
        assert_eq!(vec(1, -1, 0).reflect(Tuple::Y_UNITV), vec(1, 1, 0));
    }

    #[test]
    fn reflecting_a_vector_off_a_slanted_surface() {
        let v = -Tuple::Y_UNITV;
        let n = vec(2f64.sqrt()/2., 2f64.sqrt()/2., 0.);
        assert_abs_diff_eq!(v.reflect(n), Tuple::X_UNITV);
    }
}
