use approx::*;
use std::fmt;
use std::ops::{Add, Sub, Mul};

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Color {
    pub r: f64,
    pub g: f64,
    pub b: f64,
}

fn clamp_component(mut x: f64) -> f64 {
    x = if x < 0. { 0. } else { x };
    if x > 1. { 1. } else { x }
}

impl Color {
    pub const WHITE: Color = Color { r: 1., g: 1., b: 1. };
    pub const BLACK: Color = Color { r: 0., g: 0., b: 0. };
    pub const RED: Color = Color { r: 1., g: 0., b: 0. };
    pub const GREEN: Color = Color { r: 0., g: 1., b: 0. };
    pub const BLUE: Color = Color { r: 0., g: 0., b: 1. };
    pub const YELLOW: Color = Color { r: 1., g: 1., b: 0. };
    pub const AQUA: Color = Color { r: 0., g: 1., b: 1. };
    pub const PURPLE: Color = Color { r: 1., g: 0., b: 1. };

    fn clamp(&self) -> Color {
        Color { r: clamp_component(self.r), g: clamp_component(self.g), b: clamp_component(self.b) }
    }

    pub fn to_ppm(&self, scale: u8) -> [u8; 3] {
        let scaled = self.clamp() * (scale as f64);
        [scaled.r.round() as u8, scaled.g.round() as u8, scaled.b.round() as u8]
    }
}

impl fmt::Display for Color {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Color({}, {}, {})", self.r, self.g, self.b)
    }
}

impl AbsDiffEq for Color {
    type Epsilon = <f64 as AbsDiffEq>::Epsilon;

    fn default_epsilon() -> <f64 as AbsDiffEq>::Epsilon {
        1e-5
    }

    fn abs_diff_eq(&self, other: &Color, epsilon: <f64 as AbsDiffEq>::Epsilon) -> bool {
        f64::abs_diff_eq(&self.r, &other.r, epsilon)
            && f64::abs_diff_eq(&self.g, &other.g, epsilon)
            && f64::abs_diff_eq(&self.b, &other.b, epsilon)
    }
}

impl Add for Color {
    type Output = Color;

    fn add(self, other: Color) -> Color {
        Color { r: self.r + other.r, g: self.g + other.g, b: self.b + other.b }
    }
}

impl Add for &Color {
    type Output = Color;

    fn add(self, other: &Color) -> Color {
        Color { r: self.r + other.r, g: self.g + other.g, b: self.b + other.b }
    }
}

impl Add<Color> for &Color {
    type Output = Color;

    fn add(self, other: Color) -> Color {
        Color { r: self.r + other.r, g: self.g + other.g, b: self.b + other.b }
    }
}

impl Add<&Color> for Color {
    type Output = Color;

    fn add(self, other: &Color) -> Color {
        Color { r: self.r + other.r, g: self.g + other.g, b: self.b + other.b }
    }
}

impl Sub for Color {
    type Output = Color;

    fn sub(self, other: Color) -> Color {
        Color { r: self.r - other.r, g: self.g - other.g, b: self.b - other.b }
    }
}

impl Sub for &Color {
    type Output = Color;

    fn sub(self, other: &Color) -> Color {
        Color { r: self.r - other.r, g: self.g - other.g, b: self.b - other.b }
    }
}

impl Sub<Color> for &Color {
    type Output = Color;

    fn sub(self, other: Color) -> Color {
        Color { r: self.r - other.r, g: self.g - other.g, b: self.b - other.b }
    }
}

impl Sub<&Color> for Color {
    type Output = Color;

    fn sub(self, other: &Color) -> Color {
        Color { r: self.r - other.r, g: self.g - other.g, b: self.b - other.b }
    }
}

impl<T: Into<f64>> Mul<T> for Color {
    type Output = Color;

    fn mul(self, other: T) -> Color {
        &self * other
    }
}

impl<T: Into<f64>> Mul<T> for &Color {
    type Output = Color;

    fn mul(self, other: T) -> Color {
        let o_f = other.into();
        Color { r: self.r * o_f, g: self.g * o_f, b: self.b * o_f }
    }
}

impl Mul<Color> for f64 {
    type Output = Color;

    fn mul(self, other: Color) -> Color {
        Color { r: other.r * self, g: other.g * self, b: other.b * self }
    }
}

impl Mul<&Color> for f64 {
    type Output = Color;

    fn mul(self, other: &Color) -> Color {
        Color { r: other.r * self, g: other.g * self, b: other.b * self }
    }
}

// Hadamard Product

impl Mul for Color {
    type Output = Color;

    fn mul(self, other: Color) -> Color {
        Color { r: self.r * other.r, g: self.g * other.g, b: self.b * other.b }
    }
}

impl Mul for &Color {
    type Output = Color;

    fn mul(self, other: &Color) -> Color {
        Color { r: self.r * other.r, g: self.g * other.g, b: self.b * other.b }
    }
}

impl Mul<Color> for &Color {
    type Output = Color;

    fn mul(self, other: Color) -> Color {
        Color { r: self.r * other.r, g: self.g * other.g, b: self.b * other.b }
    }
}

impl Mul<&Color> for Color {
    type Output = Color;

    fn mul(self, other: &Color) -> Color {
        Color { r: self.r * other.r, g: self.g * other.g, b: self.b * other.b }
    }
}

pub fn color(r: impl Into<f64>, g: impl Into<f64>, b: impl Into<f64>) -> Color {
    Color { r: r.into(), g: g.into(), b: b.into() }
}

#[cfg(test)]
mod tests {
    use crate::color::*;

    #[test]
    fn color_can_be_created_by_function() {
        let c = color(-0.5, 0.4, 1.7);
        assert!(abs_diff_eq!(c.r, -0.5));
        assert!(abs_diff_eq!(c.g, 0.4));
        assert!(abs_diff_eq!(c.b, 1.7));
    }

    #[test]
    fn color_can_be_added() {
        assert!(abs_diff_eq!(color(0.9, 0.6, 0.75) + color(0.7, 0.1, 0.25), color(1.6, 0.7, 1.0)))
    }

    #[test]
    fn color_can_be_subtracted() {
        assert!(abs_diff_eq!(color(0.9, 0.6, 0.75) - color(0.7, 0.1, 0.25), color(0.2, 0.5, 0.5)))
    }

    #[test]
    fn color_can_be_scaled() {
        assert!(abs_diff_eq!(color(0.2, 0.3, 0.4) * 2., color(0.4, 0.6, 0.8)));
        assert!(abs_diff_eq!(2. * color(0.2, 0.3, 0.4), color(0.4, 0.6, 0.8)))
    }

    #[test]
    fn colors_can_take_the_hadamard_product() {
        assert!(abs_diff_eq!(color(1., 0.2, 0.4) * color(0.9, 1., 0.1), color(0.9, 0.2, 0.04)))
    }

    // This test captures the conversion process from color -> ppm, both range and rounding.
    // Changes in rounding should lead to negligible differences.
    #[test]
    fn color_can_be_formatted_as_ppm() {
        assert_eq!(color(0., 0.5, 1.).to_ppm(255), [0, 128, 255]);
    }
}
