#[macro_use]
extern crate clap;

mod tuple;
mod color;
mod matrix;
mod ray;
mod intersection;
mod pattern;
mod material;
mod shape;
mod sphere;
mod plane;
mod cube;
mod cylinder;
mod cone;
mod light;
mod world;
mod camera;
mod canvas;

fn str_to_file(s: String, f: String) -> std::io::Result<()>{
    use std::fs::File;
    use std::io::Write;

    let mut file = File::create(&f)?;
    file.write_all(s.as_bytes())?;
    Ok(())
}

#[derive(Debug, Copy, Clone)]
struct Point {
    pub x: usize,
    pub y: usize,
    pub color: color::Color
}

fn main() -> std::io::Result<()> {
    use std::f64::consts::PI;
    use tuple::{point, Tuple};
    use matrix::*;
    use color::{color, Color};
    use pattern::*;
    use shape::Shape;
    use sphere::sphere;
    use plane::plane;
    use cube::cube;
    use cylinder::cylinder;
    use cone::cone;
    use light::point_light;
    use world::world;
    use camera::camera;

    let matches = clap_app!(rust_raytrace =>
      (version: "0.0.1 alpha")
      (author: "Nathan Armstrong <nathan@functionalflame.tech")
      (about: "A ray tracer, following 'The Ray Tracer Challenge' by Jamis Buck")
      (@arg width: -w --width +takes_value "Sets the width of the canvas, in pixels")
      (@arg height: -h --height +takes_value "Sets the height of the canvas, in pixels")
      (@arg depth: --depth +takes_value "Sets the maximum ray depth (default: 5)")
    ).get_matches();
    let width =
        value_t!(matches.value_of("width"), usize)
        .unwrap_or_else(|e| panic!("Width must be specified as a positive integer: {}", e));
    let height =
        value_t!(matches.value_of("height"), usize)
        .unwrap_or_else(|e| panic!("Height must be specified as a positive integer: {}", e));
    let mut depth = None;
    if matches.is_present("depth") {
        depth = Some(value_t_or_exit!(matches.value_of("depth"), u8));
    }

    let mut floor = Box::new(plane());
    floor.material_mut().pattern =
        blended(
             perturb(
                radial_gradient(
                    solid(color(1, 0.9, 0.9)),
                    solid(color(1, 0.9, 0.9) * 0.5),
                    Blend::Linear)
                    .with_transform(rotation_z(PI / 4.).rotate_x(PI / 3.).translate(2, 0, 5)),
                0.3),
            stripe(
                solid(color(0.1, 0.5, 0.8)),
                solid(color(0.2, 0.7, 1)))
                .with_transform(rotation_y(2. * PI / 3.)),
            Blend::Linear,
            0.8);
    floor.material_mut().ambient = 0.1;
    floor.material_mut().specular = 0.05;
    floor.material_mut().diffuse = 0.9;
    floor.material_mut().reflective = 0.4;

    let mut middle = sphere();
    middle.set_transform(translation(-0.5, 1, 0.5));
    middle.material_mut().pattern =
        perturb(
            stripe(
                solid(color(0.1, 1, 0.5) * 0.1),
                solid(color(0.8, 0.6, 0.1) * 0.1))
                .with_transform(scaling(0.2, 1, 1)),
            0.1)
        .with_transform(scaling(0.5, 0.1, 0.1).rotate_y(PI / 4.));
    middle.material_mut().ambient = 0.05;
    middle.material_mut().diffuse = 0.05;
    middle.material_mut().specular = 1.;
    middle.material_mut().shininess = 300.;
    middle.material_mut().reflective = 0.9;
    middle.material_mut().transparency = 0.9;
    middle.material_mut().refractive_index = 1.52;
    middle.material_mut().casts_shadows = false;

    let mut right = cube();
    right.set_transform(scaling(0.5, 0.5, 0.5).rotate_y(PI / 5.).translate(1.5, 0.5, -0.5));
    right.material_mut().pattern =
        ping_pong_gradient(
            perturb(
                ping_pong_radial_gradient(
                    solid(color(1, 0.1, 0.5)),
                    solid(color(0.1, 0.8, 0.6)),
                    Blend::Linear),
                0.6)
                .with_transform(rotation_z(PI / 3.).rotate_x(2. * PI / 5.).scale(1.3, 1, 1).translate(-0.5, 0, 2)),
            perturb(
                ring(
                    solid(Color::BLACK),
                    solid(Color::WHITE))
                    .with_transform(rotation_z(PI / 2.).scale(0.2, 0.2, 0.2).translate(0, 5, 0)),
                0.25),
            Blend::Linear)
        .with_transform(scaling(0.1, 1, 1).translate(-1, 0, 0).rotate_x(PI / 3.).rotate_y(-2. * PI / 3.));
    right.material_mut().ambient = 0.2;
    right.material_mut().diffuse = 0.7;
    right.material_mut().specular = 0.3;
    right.material_mut().reflective = 0.1;

    let mut left = sphere();
    left.set_transform(scaling(0.33, 0.33, 0.33).translate(-1.5, 0.33, -0.75));
    left.material_mut().pattern =
        perturb(space(), 0.6)
        .with_transform(translation(-1, -1, -1).rotate_z(PI / 2.));
    left.material_mut().diffuse = 0.7;
    left.material_mut().specular = 0.3;
    left.material_mut().reflective = 0.4;

    let mut back = sphere();
    back.set_transform(scaling(2, 2, 2).translate(2, 2, 3));
    back.material_mut().pattern =
        perturb(
            checkers(
                stripe(
                    solid(color(0.5, 1, 1)),
                    solid(color(0.5, 0.5, 0)))
                    .with_transform(rotation_y(PI / 2.).rotate_z(PI / 2.).scale(0.1, 0.2, 0.1)),
                gradient(
                    solid(color(0.5, 0, 0)),
                    solid(color(0.5, 0.5, 1)),
                    Blend::Linear)),
            0.2)
        .with_transform(scaling(0.2, 0.2, 0.4));
    back.material_mut().ambient = 0.1;
    back.material_mut().diffuse = 0.1;
    back.material_mut().specular = 0.7;
    back.material_mut().reflective = 0.2;

    let mut pole = cone();
    pole.maximum = 3.;
    pole.closed = true;
    pole.set_transform(rotation_z(- PI / 4.).translate(-5, 0, 10));
    pole.material_mut().pattern =
        perturb(
            ping_pong_gradient(
                solid(Color::GREEN * 0.25),
                stripe(
                    solid(Color::WHITE),
                    solid(Color::BLACK))
                    .with_transform(rotation_x(PI / 4.).rotate_y(PI / 4.)),
                Blend::Linear),
            0.1)
        .with_transform(scaling(0.3, 0.5, 1.0).rotate_x(2. * PI / 5.).rotate_y(6. * PI / 5.));
    {
        let material = pole.material_mut();
        material.ambient = 0.1;
        material.diffuse = 0.5;
        material.specular = 0.4;
        material.shininess = 80.;
        material.reflective = 0.1;
    }

    let light = point_light(point(-10, 10, -10), Color::WHITE);
    let light2 = point_light(point(10, 10, -10), Color::RED);
    let mut world = world();
    world.lights = vec![light, light2];
    world.objects.push(floor);
    world.objects.push(Box::new(middle));
    world.objects.push(Box::new(right));
    world.objects.push(Box::new(left));
    world.objects.push(Box::new(back));
    world.objects.push(Box::new(pole));

    let mut camera = camera(width, height, PI / 3.);
    camera.transform = view_transform(
        point(0, 1.5, -5),
        point(0, 1, 0),
        Tuple::Y_UNITV
    );
    if let Some(d) = depth {
        camera.max_reflects = d;
    }

    let c = camera.render(world);
    let ppm = c.to_ppm();
    str_to_file(ppm, "/tmp/rs_raytrace.ppm".to_string())?;

    Ok(())
}
