use crate::tuple::Tuple;
use crate::matrix::M44;
use crate::ray::Ray;
use crate::intersection::{intersection, Intersections, intersections};
use crate::material::Material;
use crate::shape::{base_shape, Shape, BaseShape};
use delegate::delegate;

#[derive(Debug)]
pub struct Sphere {
    base: BaseShape
}

impl Shape for Sphere {
    delegate! {
        to self.base {
            fn transform(&self) -> &M44;
            fn set_transform(&mut self, transform: M44);
            fn material(&self) -> &Material;
            fn material_mut(&mut self) -> &mut Material;
            fn set_material(&mut self, material: Material);
        }
    }

    fn type_tag(&self) -> String { "Sphere".to_string() }

    fn local_normal_at(&self, local_point: Tuple) -> Tuple {
        local_point - Tuple::ORIGIN
    }

    fn local_intersect(&self, local_ray: &Ray) -> Intersections {
        let sphere_to_ray = local_ray.origin - Tuple::ORIGIN;
        let a = local_ray.direction * local_ray.direction;
        let b = 2. * (local_ray.direction * sphere_to_ray);
        let c = sphere_to_ray * sphere_to_ray - 1.;
        let discriminant = b * b - 4. * a * c;

        if discriminant < 0. {
            return intersections(vec![])
        }

        let t1 = (-b - discriminant.sqrt()) / (2. * a);
        let t2 = (-b + discriminant.sqrt()) / (2. * a);
        intersections(vec![intersection(t1, self), intersection(t2, self)])
    }
}

pub fn sphere() -> Sphere {
    Sphere { base: base_shape() }
}

pub fn glass_sphere() -> Sphere {
    let mut s = Sphere { base: base_shape() };
    s.material_mut().transparency = 1.;
    s.material_mut().refractive_index = 1.5;
    s.material_mut().casts_shadows = false;
    s
}

#[cfg(test)]
mod test {
    use crate::sphere::*;
    use crate::tuple::{Tuple, point, vec};
    use crate::ray::ray;
    use approx::*;

    #[test]
    fn a_helper_for_producing_a_sphere_with_a_glassy_material() {
        let s = glass_sphere();
        assert_eq!(*s.transform(), M44::IDENTITY);
        assert_eq!(s.material().transparency, 1.);
        assert_eq!(s.material().refractive_index, 1.5);
    }

    #[test]
    fn the_normal_on_a_sphere_at_a_point_on_the_x_axis() {
        let s = sphere();
        assert_eq!(s.local_normal_at(Tuple::ORIGIN + Tuple::X_UNITV), Tuple::X_UNITV);
    }

    #[test]
    fn the_normal_on_a_sphere_at_a_point_on_the_y_axis() {
        let s = sphere();
        assert_eq!(s.local_normal_at(Tuple::ORIGIN + Tuple::Y_UNITV), Tuple::Y_UNITV);
    }

    #[test]
    fn the_normal_on_a_sphere_at_a_point_on_the_z_axis() {
        let s = sphere();
        assert_eq!(s.local_normal_at(Tuple::ORIGIN + Tuple::Z_UNITV), Tuple::Z_UNITV);
    }

    #[test]
    fn the_normal_on_a_sphere_at_a_nonaxial_point() {
        let s = sphere();
        let mid = 3f64.sqrt() / 3.;
        assert_abs_diff_eq!(&s.local_normal_at(point(mid, mid, mid)), &vec(mid, mid, mid));
    }

    #[test]
    fn the_normal_is_a_normalized_vector() {
        let s = sphere();
        let mid = 3f64.sqrt() / 3.;
        let n = s.local_normal_at(point(mid, mid, mid));
        assert_abs_diff_eq!(&n.norm(), &n);
    }

    #[test]
    fn a_ray_intersects_a_sphere_at_two_points() {
        let r = ray(Tuple::ORIGIN - Tuple::Z_UNITV * 5, Tuple::Z_UNITV);
        let s = sphere();
        let xs = s.local_intersect(&r);
        assert_eq!(xs.len(), 2);
        assert_abs_diff_eq!(xs[0].t, 4.0);
        assert_abs_diff_eq!(xs[1].t, 6.0);
    }

    #[test]
    fn a_ray_intersects_a_sphere_at_a_tangent() {
        let r = ray(point(0, 1, -5), Tuple::Z_UNITV);
        let s = sphere();
        let xs = s.local_intersect(&r);
        assert_eq!(xs.len(), 2);
        assert_abs_diff_eq!(xs[0].t, 5.0);
        assert_abs_diff_eq!(xs[1].t, 5.0);
    }

    #[test]
    fn a_ray_misses_a_sphere() {
        let r = ray(point(0, 2, -5), Tuple::Z_UNITV);
        let s = sphere();
        let xs = s.local_intersect(&r);
        assert!(xs.is_empty());
    }

    #[test]
    fn a_ray_originates_inside_a_sphere() {
        let r = ray(Tuple::ORIGIN, Tuple::Z_UNITV);
        let s = sphere();
        let xs = s.local_intersect(&r);
        assert_eq!(xs.len(), 2);
        assert_abs_diff_eq!(xs[0].t, -1.0);
        assert_abs_diff_eq!(xs[1].t, 1.0);
    }

    #[test]
    fn a_sphere_is_behind_a_ray() {
        let r = ray(Tuple::ORIGIN + Tuple::Z_UNITV * 5, Tuple::Z_UNITV);
        let s = sphere();
        let xs = s.local_intersect(&r);
        assert_eq!(xs.len(), 2);
        assert_abs_diff_eq!(xs[0].t, -6.0);
        assert_abs_diff_eq!(xs[1].t, -4.0);
    }

    #[test]
    fn intersect_sets_the_object_on_intersection() {
        let r = ray(Tuple::ORIGIN - Tuple::Z_UNITV * 5, Tuple::Z_UNITV);
        let s = sphere();
        let xs = s.local_intersect(&r);
        assert_eq!(xs.len(), 2);
        assert!(std::ptr::eq(xs[0].object as *const dyn Shape as *const Sphere, &s));
        assert!(std::ptr::eq(xs[1].object as *const dyn Shape as *const Sphere, &s));
    }
}
