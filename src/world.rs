use crate::tuple::Tuple;
use crate::color::Color;
use crate::shape::Shape;
use crate::ray::{ray, Ray};
use crate::intersection::{intersections, Computations, Intersection, Intersections};
use crate::light::PointLight;

pub struct World {
    pub objects: Vec<Box<dyn Shape>>,
    pub lights: Vec<PointLight>
}

impl World {
    pub fn color_at(&self, ray: &Ray, remaining: u8) -> Color {
        let ixs = self.intersect(ray);
        let hit = match ixs.hit() {
            None => return Color::BLACK,
            Some(i) => i
        };

        let comps = hit.prepare_computations(ray, &ixs);
        self.shade_hit(&comps, remaining)
    }

    pub fn intersect(&self, ray: &Ray) -> Intersections<'_> {
        let os = &self.objects;
        let all_ixs: Vec<Intersection<'_>> =
            os
            .iter()
            .flat_map(|x| x.intersect(ray))
            .collect();

        intersections(all_ixs)
    }

    pub fn shade_hit(&self, comps: &Computations<'_>, remaining: u8) -> Color {
        let mat = comps.object.material();

        self.lights
            .iter()
            .fold(
                Color::BLACK,
                |acc, light| {
                    let shadowed = self.is_shadowed(light, comps.over_point);
                    let surface = mat.lighting(comps.object, light, comps.over_point, comps.eyev, comps.normalv, shadowed);
                    let reflected = self.reflected_color(&comps, remaining);
                    let refracted = self.refracted_color(&comps, remaining);

                    let material = comps.object.material();
                    let refs = if material.reflective > 0. && material.transparency > 0. {
                        let reflectance = comps.schlick();
                        reflected * reflectance + refracted * (1. - reflectance)
                    } else {
                        reflected + refracted
                    };

                    acc + surface + refs
                }
            )
    }

    fn is_shadowed(&self, light: &PointLight, point: Tuple) -> bool {
        let point_to_light = light.position - point;
        let distance = point_to_light.mag();
        let r = ray(point, point_to_light.norm());
        let ixs = self.intersect(&r);

        match ixs.hit() {
            Some(ix) => ix.t < distance && ix.object.material().casts_shadows,
            None => false
        }
    }

    fn reflected_color(&self, comps: &Computations, remaining: u8) -> Color {
        let reflective = comps.object.material().reflective;

        if remaining == 0 || reflective == 0. {
            Color::BLACK
        } else {
            let reflect_ray = ray(comps.over_point, comps.reflectv);
            self.color_at(&reflect_ray, remaining - 1) * reflective
        }
    }

    fn refracted_color(&self, comps: &Computations, remaining: u8) -> Color {
        let transparency = comps.object.material().transparency;
        if remaining == 0 || transparency == 0. {
            return Color::BLACK;
        }

        let n_ratio = comps.n1 / comps.n2;
        let cos_i = comps.eyev * comps.normalv;
        let sin2_t = n_ratio * n_ratio * (1. - cos_i * cos_i);

        if sin2_t > 1. {
            return Color::BLACK;
        }

        let cos_t = (1. - sin2_t).sqrt();
        let direction =
            comps.normalv * (n_ratio * cos_i - cos_t) -
            comps.eyev * n_ratio;
        let refract_ray = ray(comps.under_point, direction);

        self.color_at(&refract_ray, remaining - 1) * transparency
    }
}

pub fn world() -> World {
    World { objects: vec![], lights: vec![] }
}

pub fn default_world() -> World {
    use crate::tuple::point;
    use crate::color::color;
    use crate::matrix::scaling;
    use crate::pattern::solid;
    use crate::light::point_light;
    use crate::material::material;
    use crate::sphere::sphere;

    let light = point_light(point(-10, 10, -10), Color::WHITE);

    let mut m1 = material();
    m1.pattern = solid(color(0.8, 1, 0.6));
    m1.diffuse = 0.7;
    m1.specular = 0.2;
    let mut s1 = sphere();
    s1.set_material(m1);

    let mut s2 = sphere();
    s2.set_transform(scaling(0.5, 0.5, 0.5));

    let mut w = world();
    w.objects.push(Box::new(s1));
    w.objects.push(Box::new(s2));
    w.lights = vec![light];

    w
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::tuple::{point, vec, Tuple};
    use crate::color::{color, Color};
    use crate::matrix::{scaling, translation, M44};
    use crate::light::point_light;
    use crate::pattern::solid;
    use crate::material::material;
    use crate::sphere::sphere;
    use crate::plane::plane;
    use crate::ray::ray;
    use crate::intersection::intersection;
    use approx::*;

    #[test]
    fn creating_a_world() {
        let world = world();
        assert!(world.objects.is_empty());
        assert!(world.lights.is_empty());
    }

    #[test]
    fn the_default_world() {
        let world = default_world();
        assert_eq!(world.lights, vec![point_light(point(-10, 10, -10), Color::WHITE)]);

        let mut m1 = material();
        m1.pattern = solid(color(0.8, 1, 0.6));
        m1.diffuse = 0.7;
        m1.specular = 0.2;
        assert!(
            world
                .objects
                .iter()
                .any(|e|
                     e.type_tag() == "Sphere"
                     && *e.transform() == M44::IDENTITY
                     && *e.material() == m1
                ));

        assert!(
            world
                .objects
                .iter()
                .any(|e|
                     e.type_tag() == "Sphere"
                     && *e.transform() == scaling(0.5, 0.5, 0.5)
                     && *e.material() == material()
                ));
    }

    #[test]
    fn intersect_a_world_with_a_ray() {
        let w = default_world();
        let r = ray(point(0, 0, -5), Tuple::Z_UNITV);
        let xs = w.intersect(&r);
        assert_eq!(xs.len(), 4);
        assert_abs_diff_eq!(xs[0].t, 4.);
        assert_abs_diff_eq!(xs[1].t, 4.5);
        assert_abs_diff_eq!(xs[2].t, 5.5);
        assert_abs_diff_eq!(xs[3].t, 6.);
    }

    #[test]
    fn there_is_no_shadow_when_nothing_is_collinear_with_point_and_light() {
        let w = default_world();
        let l = &w.lights[0];
        let p = point(0, 10, 0);
        assert!(!w.is_shadowed(l, p));
    }

    #[test]
    fn the_shadow_when_an_object_is_between_the_point_and_the_light() {
        let w = default_world();
        let l = &w.lights[0];
        let p = point(10, -10, 10);
        assert!(w.is_shadowed(l, p));
    }

    #[test]
    fn there_is_no_shadow_when_an_object_is_behind_the_light() {
        let w = default_world();
        let l = &w.lights[0];
        let p = point(-20, 20, -20);
        assert!(!w.is_shadowed(l, p));
    }

    #[test]
    fn there_is_no_shadow_when_an_object_is_behind_the_point() {
        let w = default_world();
        let l = &w.lights[0];
        let p = point(-2, 2, -2);
        assert!(!w.is_shadowed(l, p));
    }

    #[test]
    fn shading_an_intersection() {
        let w = default_world();
        let r = ray(point(0, 0, -5), Tuple::Z_UNITV);
        let ix = intersection(4., &*w.objects[0]);
        let ixs = intersections(vec![ix]);
        let comps = ixs[0].prepare_computations(&r, &ixs);
        let c = w.shade_hit(&comps, 0);
        assert_abs_diff_eq!(c, color(0.38066, 0.47583, 0.2855));
    }

    #[test]
    fn shading_an_intersection_from_the_inside() {
        let mut w = default_world();
        w.lights = vec![point_light(point(0., 0.25, 0.), Color::WHITE)];

        let r = ray(Tuple::ORIGIN, Tuple::Z_UNITV);
        let ix = intersection(0.5, &*w.objects[1]);
        let ixs = intersections(vec![ix]);
        let comps = ixs[0].prepare_computations(&r, &ixs);
        let c = w.shade_hit(&comps, 0);
        assert_abs_diff_eq!(c, color(0.90498, 0.90498, 0.90498));
    }

    #[test]
    fn the_color_when_a_ray_misses() {
        let w = default_world();
        let r = ray(point(0, 0, -5), Tuple::Y_UNITV);
        let c = w.color_at(&r, 0);
        assert_abs_diff_eq!(c, Color::BLACK);
    }

    #[test]
    fn the_color_when_a_ray_hits() {
        let w = default_world();
        let r = ray(point(0, 0, -5), Tuple::Z_UNITV);
        let c = w.color_at(&r, 0);
        assert_abs_diff_eq!(c, color(0.38066, 0.47583, 0.2855));
    }

    #[test]
    fn the_color_with_an_intersection_behind_the_ray() {
        let mut w = default_world();
        let inner_mat_color = color(0.6, 0.7, 0.8);

        {
            let outer = &mut *w.objects[0];
            outer.material_mut().ambient = 1.;

            let inner = &mut *w.objects[1];
            inner.material_mut().ambient = 1.;
            inner.material_mut().pattern = solid(inner_mat_color);
        }

        let r = ray(point(0, 0, 0.75), -Tuple::Z_UNITV);

        let c = w.color_at(&r, 0);
        assert_abs_diff_eq!(c, inner_mat_color);
    }

    #[test]
    fn shade_hit_is_given_an_intersection_in_shadow() {
        let mut sphere2 = sphere();
        sphere2.set_transform(translation(0, 0, 10));

        let mut w = world();
        w.lights.push(point_light(point(0, 0, -10), Color::WHITE));
        w.objects.push(Box::new(sphere()));
        w.objects.push(Box::new(sphere2));

        let r = ray(point(0, 0, 5), Tuple::Z_UNITV);
        let ix = intersection(4., &*w.objects[1]);

        let ixs = intersections(vec![ix]);
        let comps = ixs[0].prepare_computations(&r, &ixs);
        let c = w.shade_hit(&comps, 0);

        assert_abs_diff_eq!(c, Color::WHITE * 0.1);
    }

    #[test]
    fn the_reflected_color_for_a_nonreflective_material() {
        let mut w = default_world();
        w.objects[1].material_mut().ambient = 1.;

        let r = ray(Tuple::ORIGIN, Tuple::Z_UNITV);
        let ix = intersection(1., &*w.objects[1]);
        let ixs = intersections(vec![ix]);
        let comps = ixs[0].prepare_computations(&r, &ixs);
        let color = w.reflected_color(&comps, 0);

        assert_eq!(color, Color::BLACK);
    }

    #[test]
    fn the_reflected_color_for_a_reflective_material() {
        let mut shape = plane();
        shape.material_mut().reflective = 0.5;
        shape.set_transform(translation(0, -1, 0));

        let mut w = default_world();
        w.objects.push(Box::new(shape));

        let r = ray(point(0, 0, -3), vec(0, -(2f64.sqrt() / 2.), 2f64.sqrt() / 2.));
        let ix = intersection(2f64.sqrt(), &**w.objects.last().unwrap());
        let ixs = intersections(vec![ix]);
        let comps = ixs[0].prepare_computations(&r, &ixs);
        let c = w.reflected_color(&comps, 5);

        assert_abs_diff_eq!(c, color(0.19032, 0.2379, 0.14274), epsilon = 1e-4);
    }

    #[test]
    fn shade_hit_with_a_reflective_material() {
        let mut shape = plane();
        shape.material_mut().reflective = 0.5;
        shape.set_transform(translation(0, -1, 0));

        let mut w = default_world();
        w.objects.push(Box::new(shape));

        let r = ray(point(0, 0, -3), vec(0, -(2f64.sqrt() / 2.), 2f64.sqrt() / 2.));
        let ix = intersection(2f64.sqrt(), &**w.objects.last().unwrap());
        let ixs = intersections(vec![ix]);
        let comps = ixs[0].prepare_computations(&r, &ixs);
        let c = w.shade_hit(&comps, 5);

        assert_abs_diff_eq!(c, color(0.87677, 0.92436, 0.82918), epsilon = 1e-4);
    }

    #[test]
    fn color_at_with_mutually_reflective_surfaces() {
        let mut lower = plane();
        lower.material_mut().reflective = 1.;
        lower.set_transform(translation(0, -1, 0));

        let mut upper = plane();
        upper.material_mut().reflective = 1.;
        upper.set_transform(translation(0, 1, 0));

        let mut w = world();
        w.objects.push(Box::new(lower));
        w.objects.push(Box::new(upper));
        w.lights.push(point_light(Tuple::ORIGIN, Color::WHITE));

        let r = ray(Tuple::ORIGIN, Tuple::Y_UNITV);
        w.color_at(&r, 5);

        assert!(true, "The above must terminate")
    }

    #[test]
    fn the_reflected_color_at_the_maximum_recursive_depth() {
        let mut shape = plane();
        shape.material_mut().reflective = 0.5;
        shape.set_transform(translation(0, -1, 0));

        let mut w = default_world();
        w.objects.push(Box::new(shape));

        let r = ray(point(0, 0, -3), vec(0, -(2f64.sqrt() / 2.), 2f64.sqrt() / 2.));
        let ix = intersection(2f64.sqrt(), &*w.objects[0]);
        let ixs = intersections(vec![ix]);
        let comps = ixs[0].prepare_computations(&r, &ixs);
        let c = w.reflected_color(&comps, 0);

        assert_eq!(c, Color::BLACK);
    }

    #[test]
    fn the_refracted_color_with_an_opaque_surface() {
        let w = default_world();
        let shape = &w.objects[0];
        let r = ray(point(0, 0, -5), Tuple::Z_UNITV);
        let ixs = intersections(vec![
            intersection(4., &**shape),
            intersection(6., &**shape)
        ]);
        let comps = ixs[0].prepare_computations(&r, &ixs);
        let c = w.refracted_color(&comps, 5);

        assert_eq!(c, Color::BLACK);
    }

    #[test]
    fn the_refracted_color_at_the_maximum_recursive_depth() {
        let mut w = default_world();
        {
            let shape = &mut w.objects[0];
            shape.material_mut().transparency = 1.;
            shape.material_mut().refractive_index = 1.5;
        }
        let shape = &w.objects[0];

        let r = ray(point(0, 0, -5), Tuple::Z_UNITV);
        let ixs = intersections(vec![
            intersection(4., &**shape),
            intersection(6., &**shape)
        ]);
        let comps = ixs[0].prepare_computations(&r, &ixs);
        let c = w.refracted_color(&comps, 0);

        assert_eq!(c, Color::BLACK);
    }

    #[test]
    fn the_refracted_color_under_total_internal_reflection() {
        let mut w = default_world();
        {
            let shape = &mut w.objects[0];
            shape.material_mut().transparency = 1.;
            shape.material_mut().refractive_index = 1.5;
        }
        let shape = &w.objects[0];

        let r = ray(point(0, 0, 2f64.sqrt() / 2.), Tuple::Y_UNITV);
        let ixs = intersections(vec![
            intersection(2f64.sqrt() / -2., &**shape),
            intersection(2f64.sqrt() / 2., &**shape)
        ]);
        let comps = ixs[1].prepare_computations(&r, &ixs);
        let c = w.refracted_color(&comps, 0);

        assert_eq!(c, Color::BLACK);
    }

    #[test]
    fn the_refracted_color_with_a_refracted_ray() {
        use crate::pattern::test::test_pattern;

        let mut w = default_world();
        {
            let shape = &mut w.objects[0];
            shape.material_mut().ambient = 1.;
            shape.material_mut().pattern = test_pattern();

            let shape2 = &mut w.objects[1];
            shape2.material_mut().transparency = 1.0;
            shape2.material_mut().refractive_index = 1.5;
        }
        let a = &w.objects[0];
        let b = &w.objects[1];

        let r = ray(point(0, 0, 0.1), Tuple::Y_UNITV);
        let ixs = intersections(vec![
            intersection(-0.9899, &**a),
            intersection(-0.4899, &**b),
            intersection(0.4899, &**b),
            intersection(0.9899, &**a)
        ]);
        let comps = ixs[2].prepare_computations(&r, &ixs);
        let c = w.refracted_color(&comps, 5);

        assert_abs_diff_eq!(c, color(0, 0.99888, 0.04725), epsilon = 1e-4);
    }

    #[test]
    fn shade_hit_with_a_transparent_material() {
        let mut floor = plane();
        floor.set_transform(translation(0, -1, 0));
        floor.material_mut().transparency = 0.5;
        floor.material_mut().refractive_index = 1.5;

        let mut ball = sphere();
        ball.set_transform(translation(0, -3.5, -0.5));
        ball.material_mut().pattern = solid(Color::RED);
        ball.material_mut().ambient = 0.5;

        let mut w = default_world();
        w.objects.push(Box::new(ball));
        w.objects.push(Box::new(floor));

        let r = ray(point(0, 0, -3), vec(0, 2f64.sqrt() / -2., 2f64.sqrt() / 2.));
        let ixs = intersections(vec![intersection(2f64.sqrt(), &**w.objects.last().unwrap())]);
        let comps = ixs[0].prepare_computations(&r, &ixs);
        let c = w.shade_hit(&comps, 5);

        assert_abs_diff_eq!(c, color(0.93642, 0.68642, 0.68642), epsilon = 1e-4);
    }

    #[test]
    fn shade_hit_with_a_reflective_transparent_material() {
        let mut floor = plane();
        floor.set_transform(translation(0, -1, 0));
        floor.material_mut().reflective = 0.5;
        floor.material_mut().transparency = 0.5;
        floor.material_mut().refractive_index = 1.5;

        let mut ball = sphere();
        ball.set_transform(translation(0, -3.5, -0.5));
        ball.material_mut().pattern = solid(Color::RED);
        ball.material_mut().ambient = 0.5;

        let mut w = default_world();
        w.objects.push(Box::new(ball));
        w.objects.push(Box::new(floor));

        let r = ray(point(0, 0, -3), vec(0, 2f64.sqrt() / -2., 2f64.sqrt() / 2.));
        let ixs = intersections(vec![intersection(2f64.sqrt(), &**w.objects.last().unwrap())]);
        let comps = ixs[0].prepare_computations(&r, &ixs);
        let c = w.shade_hit(&comps, 5);

        assert_abs_diff_eq!(c, color(0.93391, 0.69643, 0.69243), epsilon = 1e-4);
    }
}
