use crate::tuple::Tuple;
use crate::matrix::M44;

#[derive(Debug, Copy, Clone)]
pub struct Ray {
    pub origin: Tuple,
    pub direction: Tuple
}

impl Ray {
    pub fn position(&self, t: f64) -> Tuple {
        self.origin + self.direction * t
    }

    pub fn transform(&self, m: &M44) -> Ray {
        ray(m * self.origin, m * self.direction)
    }
}

pub fn ray(origin: Tuple, direction: Tuple) -> Ray {
    Ray { origin, direction }
}

#[cfg(test)]
mod test {
    use crate::ray::*;
    use crate::tuple::{Tuple, point, vec};
    use crate::matrix::{translation, scaling};

    #[test]
    fn constructing_and_querying_a_ray() {
        let origin = point(1, 2, 3);
        let direction = vec(4, 5, 6);
        let r = ray(origin, direction);
        assert_eq!(origin, r.origin);
        assert_eq!(direction, r.direction);
    }

    #[test]
    fn computing_a_point_from_a_distance() {
        let r = ray(point(2, 3, 4), Tuple::X_UNITV);
        assert_eq!(r.position(0.), point(2, 3, 4));
        assert_eq!(r.position(1.), point(3, 3, 4));
        assert_eq!(r.position(-1.), point(1, 3, 4));
        assert_eq!(r.position(2.5), point(4.5, 3, 4));
    }

    #[test]
    fn translating_a_ray() {
        let r1 = ray(point(1, 2, 3), Tuple::Y_UNITV);
        let m = translation(3, 4, 5);
        let r2 = r1.transform(&m);
        assert_eq!(r2.origin, point(4, 6, 8));
        assert_eq!(r2.direction, Tuple::Y_UNITV);
    }

    #[test]
    fn scaling_a_ray() {
        let r1 = ray(point(1, 2, 3), Tuple::Y_UNITV);
        let m = scaling(2, 3, 4);
        let r2 = r1.transform(&m);
        assert_eq!(r2.origin, point(2, 6, 12));
        assert_eq!(r2.direction, Tuple::Y_UNITV * 3);
    }
}
