use crate::tuple::{vec, Tuple};
use crate::matrix::M44;
use crate::material::Material;
use crate::shape::{base_shape, Shape, BaseShape};
use crate::ray::Ray;
use crate::intersection::{intersection, intersections, Intersections};
use delegate::delegate;

#[derive(Debug)]
pub struct Cube {
    base: BaseShape
}

impl Cube {
    fn check_axis(origin: f64, direction: f64) -> (f64, f64) {
        let tmin = (-1. - origin) / direction;
        let tmax = (1. - origin) / direction;

        if tmin > tmax {
            (tmax, tmin)
        } else {
            (tmin, tmax)
        }
    }
}

impl Shape for Cube {
    delegate! {
        to self.base {
            fn transform(&self) -> &M44;
            fn set_transform(&mut self, transform: M44);
            fn material(&self) -> &Material;
            fn material_mut(&mut self) -> &mut Material;
            fn set_material(&mut self, material: Material);
        }
    }

    fn type_tag(&self) -> String { "Cube".to_string() }

    fn local_normal_at(&self, local_point: Tuple) -> Tuple {
        let a_x = local_point.x.abs();
        let a_y = local_point.y.abs();
        let a_z = local_point.z.abs();
        let maxc = f64::max(f64::max(a_x, a_y), a_z);

        if maxc == a_x {
            vec(local_point.x, 0, 0)
        } else if maxc == a_y  {
            vec(0, local_point.y, 0)
        } else {
            vec(0, 0, local_point.z)
        }
    }

    fn local_intersect(&self, local_ray: &Ray) -> Intersections {
        let (xtmin, xtmax) = Self::check_axis(local_ray.origin.x, local_ray.direction.x);
        let (ytmin, ytmax) = Self::check_axis(local_ray.origin.y, local_ray.direction.y);
        let (ztmin, ztmax) = Self::check_axis(local_ray.origin.z, local_ray.direction.z);

        let tmin = f64::max(f64::max(xtmin, ytmin), ztmin);
        let tmax = f64::min(f64::min(xtmax, ytmax), ztmax);

        let ixs = if tmin > tmax {
            vec![]
        } else {
            vec![
                intersection(tmin, self),
                intersection(tmax, self)
            ]
        };

        intersections(ixs)
    }
}

pub fn cube() -> Cube {
    Cube { base: base_shape() }
}

#[cfg(test)]
mod test {
    use crate::cube::*;
    use crate::tuple::{point, vec, Tuple};
    use crate::ray::ray;
    use approx::*;

    #[test]
    fn a_ray_intersects_a_cube() {
        let c = cube();

        let cases = [
            ("+x",     point( 5  ,  0.5,  0), -Tuple::X_UNITV,  4., 6.),
            ("-x",     point(-5  ,  0.5,  0),  Tuple::X_UNITV,  4., 6.),
            ("+y",     point( 0.5,  5  ,  0), -Tuple::Y_UNITV,  4., 6.),
            ("-y",     point( 0.5, -5  ,  0),  Tuple::Y_UNITV,  4., 6.),
            ("+z",     point( 0.5,  0  ,  5), -Tuple::Z_UNITV,  4., 6.),
            ("-z",     point( 0.5,  0  , -5),  Tuple::Z_UNITV,  4., 6.),
            ("inside", point( 0  ,  0.5,  0),  Tuple::Z_UNITV, -1., 1.)
        ];

        for (case_name, origin, direction, t1, t2) in cases.iter() {
            let r = ray(*origin, *direction);
            let ixs = c.local_intersect(&r);

            assert_eq!(ixs.len(), 2, "Case: {} - Expected len 2, got {}", case_name, ixs.len());
            assert!(
                abs_diff_eq!(ixs[0].t, t1),
                "Case: {} - expected t1 {}, got {}", case_name, t1, ixs[0].t);
            assert!(
                abs_diff_eq!(ixs[1].t, t2),
                "Case: {} - expected t2 {}, got {}", case_name, t2, ixs[1].t);
        }
    }

    #[test]
    fn a_ray_misses_a_cube() {
        let c = cube();

        let cases = [
            (point(-2,  0,  0), vec(0.2673, 0.5345, 0.8018)),
            (point( 0, -2,  0), vec(0.8018, 0.2673, 0.5345)),
            (point( 0,  0, -2), vec(0.5345, 0.8018, 0.2673)),
            (point( 2,  0,  2), -Tuple::Z_UNITV),
            (point( 0,  2,  2), -Tuple::Y_UNITV),
            (point( 2,  2,  0), -Tuple::X_UNITV)
        ];

        for (i, (origin, direction)) in cases.iter().enumerate() {
            let r = ray(*origin, *direction);
            let ixs = c.local_intersect(&r);

            assert_eq!(ixs.len(), 0, "Case #{} - Expected no intersections, got {}", i, ixs.len());
        }
    }

    #[test]
    fn the_normal_on_the_surface_of_a_cube() {
        let c = cube();

        let cases = [
            (point( 1  ,  0.5, -0.8),  Tuple::X_UNITV),
            (point(-1  ,  0.2,  0.9), -Tuple::X_UNITV),
            (point(-0.4,  1  , -0.1),  Tuple::Y_UNITV),
            (point( 0.3, -1  , -0.7), -Tuple::Y_UNITV),
            (point(-0.6,  0.3,  1  ),  Tuple::Z_UNITV),
            (point( 0.4,  0.4, -1  ), -Tuple::Z_UNITV),
            (point( 1  ,  1  ,  1  ),  Tuple::X_UNITV),
            (point(-1  , -1  , -1  ), -Tuple::X_UNITV),
        ];

        for (i, (p, normal)) in cases.iter().enumerate() {
            let calculated_normal = c.local_normal_at(*p);
            assert!(
                abs_diff_eq!(calculated_normal, normal),
                "Case #{} - expected normal {:?}, got {:?}", i, normal, calculated_normal);
        }
    }
}
