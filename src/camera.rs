use crate::tuple::{point, Tuple};
use crate::matrix::M44;
use crate::color::Color;
use crate::ray::{ray, Ray};
use crate::world::World;
use crate::canvas::{canvas, Canvas};

#[derive(Debug, PartialEq, Clone)]
pub struct Camera {
    pub hsize: usize,
    pub vsize: usize,
    pub field_of_view: f64,
    pub transform: M44,
    pub half_width: f64,
    pub half_height: f64,
    pub pixel_size: f64,
    pub max_reflects: u8
}

#[derive(Debug, Copy, Clone)]
struct Point {
    pub x: usize,
    pub y: usize,
    pub color: Color
}

impl Camera {
    const DEFAULT_MAX_REFLECTS: u8 = 5;

    pub fn render(&self, world: World) -> Canvas {
        use rayon::prelude::*;

        let mut image = canvas(self.hsize, self.vsize);

        (0..(self.hsize * self.vsize))
            .into_par_iter()
            .map(|i| {
                let (x, y) = (i / self.vsize, i % self.vsize);
                let ray = self.ray_for_pixel(x, y);
                let color = world.color_at(&ray, self.max_reflects);
                Point { x: x, y: y, color: color }
            })
            .collect::<Vec<Point>>()
            .iter()
            .for_each(|p| image.write_px(p.x, p.y, p.color));

        image
    }

    fn ray_for_pixel(&self, px: usize, py: usize) -> Ray {
        let offset = ((px as f64 + 0.5) * self.pixel_size,
                      (py as f64 + 0.5) * self.pixel_size);
        let world = (self.half_width - offset.0, self.half_height - offset.1);
        let inverse_transform = self.transform.inverse();

        let pixel = &inverse_transform * point(world.0, world.1, -1);
        let origin = inverse_transform * Tuple::ORIGIN;
        let direction = (pixel - origin).norm();

        ray(origin, direction)
    }
}

pub fn camera(hsize: usize, vsize: usize, field_of_view: impl Into<f64>) -> Camera {
    let fov = field_of_view.into();
    let half_view = (fov / 2.).tan();
    let aspect = (hsize as f64) / (vsize as f64);
    let (half_width, half_height) =
        if aspect >= 1. {
            (half_view, half_view / aspect)
        } else {
            (half_view * aspect, half_view)
        };
    let pixel_size = (half_width * 2.) / (hsize as f64);

    Camera {
        hsize: hsize,
        vsize: vsize,
        field_of_view: fov,
        transform: M44::IDENTITY,
        half_width: half_width,
        half_height: half_height,
        pixel_size: pixel_size,
        max_reflects: Camera::DEFAULT_MAX_REFLECTS
    }
}

#[cfg(test)]
mod tests {
    use std::f64::consts::PI;
    use crate::camera::*;
    use crate::tuple::{point, vec, Tuple};
    use crate::matrix::{translation, view_transform};
    use crate::color::color;
    use crate::world::default_world;
    use approx::*;

    #[test]
    fn constructing_a_camera() {
        let hsize = 160;
        let vsize = 120;
        let field_of_view = PI / 2.;
        let c = camera(hsize, vsize, field_of_view);
        assert_eq!(c.hsize, hsize);
        assert_eq!(c.vsize, vsize);
        assert_eq!(c.field_of_view, field_of_view);
        assert_eq!(c.transform, M44::IDENTITY);
    }

    #[test]
    fn the_pixel_size_for_a_horizontal_canvas() {
        let c = camera(200, 125, PI / 2.);
        assert_abs_diff_eq!(c.pixel_size, 0.01);
    }

    #[test]
    fn the_pixel_size_for_a_vertical_canvas() {
        let c = camera(125, 200, PI / 2.);
        assert_abs_diff_eq!(c.pixel_size, 0.01);
    }

    #[test]
    fn constructing_a_ray_through_the_center_of_the_canvas() {
        let c = camera(201, 101, PI / 2.);
        let r = c.ray_for_pixel(100, 50);
        assert_abs_diff_eq!(r.origin, Tuple::ORIGIN);
        assert_abs_diff_eq!(r.direction, -Tuple::Z_UNITV);
    }

    #[test]
    fn constructing_a_ray_through_a_corner_of_the_canvas() {
        let c = camera(201, 101, PI / 2.);
        let r = c.ray_for_pixel(0, 0);
        assert_abs_diff_eq!(r.origin, Tuple::ORIGIN);
        assert_abs_diff_eq!(r.direction, vec(0.66519, 0.33259, -0.66851), epsilon = 1e-5);
    }

    #[test]
    fn constructing_a_ray_when_the_camera_is_transformed() {
        let mut c = camera(201, 101, PI / 2.);
        c.transform = translation(0, -2, 5).rotate_y(PI / 4.);

        let r = c.ray_for_pixel(100, 50);
        assert_abs_diff_eq!(r.origin, point(0, 2, -5));
        assert_abs_diff_eq!(r.direction, (Tuple::X_UNITV - Tuple::Z_UNITV) * ((2 as f64).sqrt() / 2.));
    }

    #[test]
    fn rendering_a_world_with_a_camera() {
        let w = default_world();

        let from = Tuple::ORIGIN - Tuple::Z_UNITV * 5;
        let to = Tuple::ORIGIN;
        let up = Tuple::Y_UNITV;
        let mut c = camera(11, 11, PI / 2.);
        c.transform = view_transform(from, to, up);

        let image = c.render(w);
        assert_abs_diff_eq!(image.read_px(5, 5), color(0.38066, 0.47583, 0.2855));
    }
}
