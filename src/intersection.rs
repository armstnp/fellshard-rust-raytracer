use crate::tuple::Tuple;
use crate::shape::Shape;
use crate::ray::Ray;
use std::ops::Index;
use std::cmp::Ordering;
use std::vec;
use std::ptr;

pub struct Computations<'a> {
    pub t: f64,
    pub object: &'a dyn Shape,
    pub point: Tuple,
    pub over_point: Tuple,
    pub under_point: Tuple,
    pub eyev: Tuple,
    pub normalv: Tuple,
    pub reflectv: Tuple,
    pub inside: bool,
    pub n1: f64,
    pub n2: f64
}

impl Computations<'_> {
    pub fn schlick(&self) -> f64 {
        let mut cos = self.eyev * self.normalv;

        // Total internal reflection can only occur if n1 > n2
        if self.n1 > self.n2 {
            let n = self.n1 / self.n2;
            let sin2_t = n * n * (1. - cos * cos);
            if sin2_t > 1. {
                return 1.;
            }

            cos = (1. - sin2_t).sqrt();
        }

        let mut r0 = (self.n1 - self.n2) / (self.n1 + self.n2);
        r0 *= r0;

        r0 + (1. - r0) * (1. - cos).powf(5.)
    }
}

pub struct Intersection<'a> {
    pub t: f64,
    pub object: &'a dyn Shape
}

impl Intersection<'_> {
    const EPSILON: f64 = 1e-7;

    pub fn prepare_computations(&self, ray: &Ray, ixs: &Intersections) -> Computations {
        let point = ray.position(self.t);
        let eyev = -ray.direction;
        let mut normalv = self.object.normal_at(point);
        let inside = normalv * eyev < 0.;

        if inside {
            normalv = -normalv;
        }

        let over_point = point + normalv * Self::EPSILON;
        let under_point = point - normalv * Self::EPSILON;
        let reflectv = ray.direction.reflect(normalv);
        let (n1, n2) = self.find_refractive_indices(ixs);

        Computations {
            t: self.t,
            object: self.object,
            point: point,
            over_point: over_point,
            under_point: under_point,
            eyev: eyev,
            normalv: normalv,
            reflectv: reflectv,
            inside: inside,
            n1: n1,
            n2: n2
        }
    }

    fn find_refractive_indices(&self, ixs: &Intersections) -> (f64, f64) {
        let mut containers: Vec<&dyn Shape> = vec![];
        let mut n1 = 1.;
        let mut n2 = 1.;

        for ix in &ixs.0 {
            let is_hit = ptr::eq(ix, self);

            if is_hit {
                n1 = containers.last().map_or(1., |o| o.material().refractive_index);
            }

            if containers.iter().any(|o| ptr::eq(*o, ix.object)) {
                containers.retain(|o| !ptr::eq(*o, ix.object));
                // It's not detecting it at all...
            } else {
                containers.push(ix.object);
            }

            if is_hit {
                n2 = containers.last().map_or(1., |o| o.material().refractive_index);
                break;
            }
        }

        (n1, n2)
    }
}

impl PartialEq for Intersection<'_> {
    fn eq(&self, other: &Self) -> bool {
        self.t.eq(&other.t)
    }
}

impl PartialOrd for Intersection<'_> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.t.partial_cmp(&other.t)
    }
}

pub fn intersection(t: f64, object: &dyn Shape) -> Intersection {
    Intersection { t, object }
}

pub struct Intersections<'a>(Vec<Intersection<'a>>);

impl<'a> Index<usize> for Intersections<'a> {
    type Output = Intersection<'a>;

    fn index<'b>(&self, i: usize) -> &Intersection<'a> {
        &self.0[i]
    }
}

impl<'a> Intersections<'a> {
    pub fn len(&self) -> usize {
        self.0.len()
    }

    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }

    pub fn hit(&self) -> Option<&Intersection<'a>> {
        self.0.iter().find(|x| x.t.is_sign_positive())
    }
}

pub fn intersections<'a>(mut xs: Vec<Intersection<'a>>) -> Intersections<'a> {
    xs.sort_by(|a, b| a.partial_cmp(b).unwrap());
    Intersections(xs)
}

impl<'a> IntoIterator for Intersections<'a> {
    type Item = Intersection<'a>;
    type IntoIter = vec::IntoIter<Intersection<'a>>;

    /// Creates a consuming iterator over this collection's intersections.
    fn into_iter(self) -> vec::IntoIter<Intersection<'a>> {
        self.0.into_iter()
    }
}

#[cfg(test)]
mod test {
    use crate::matrix::{scaling, translation};
    use crate::intersection::*;
    use crate::sphere::{sphere, glass_sphere};
    use crate::plane::plane;
    use crate::ray::ray;
    use crate::tuple::{point, vec, Tuple};
    use approx::*;

    #[test]
    fn an_intersection_encapsulates_t_and_object() {
        let s = sphere();
        let i = intersection(3.5, &s);
        assert_eq!(i.t, 3.5);
        assert!(std::ptr::eq(i.object, &s));
    }

    #[test]
    fn precomputing_the_state_of_an_intersection() {
        let r = ray(point(0., 0., -5.), Tuple::Z_UNITV);
        let shape = sphere();
        let ix = intersection(4., &shape);
        let ixs = intersections(vec![ix]);
        let comps = ixs[0].prepare_computations(&r, &ixs);
        assert!(std::ptr::eq(ixs[0].object, comps.object));
        assert_eq!(comps.point, Tuple::ORIGIN - Tuple::Z_UNITV);
        assert_eq!(comps.eyev, -Tuple::Z_UNITV);
        assert_eq!(comps.normalv, -Tuple::Z_UNITV);
    }

    #[test]
    fn the_hit_when_an_intersection_occurs_on_the_outside() {
        let r = ray(point(0., 0., -5.), Tuple::Z_UNITV);
        let shape = sphere();
        let ix = intersection(4., &shape);
        let ixs = intersections(vec![ix]);
        let comps = ixs[0].prepare_computations(&r, &ixs);
        assert!(!comps.inside);
    }

    #[test]
    fn the_hit_when_an_intersection_occurs_on_the_inside() {
        let r = ray(Tuple::ORIGIN, Tuple::Z_UNITV);
        let shape = sphere();
        let ix = intersection(1., &shape);
        let ixs = intersections(vec![ix]);
        let comps = ixs[0].prepare_computations(&r, &ixs);
        assert_eq!(comps.point, Tuple::ORIGIN + Tuple::Z_UNITV);
        assert_eq!(comps.eyev, -Tuple::Z_UNITV);
        assert!(comps.inside);
        assert_eq!(comps.normalv, -Tuple::Z_UNITV);
    }

    #[test]
    fn aggregating_intersections() {
        let s = sphere();
        let i1 = intersection(1., &s);
        let i2 = intersection(2., &s);
        let xs = intersections(vec![i1, i2]);
        assert_eq!(xs.len(), 2);
        assert_eq!(xs[0].t, 1.);
        assert_eq!(xs[1].t, 2.);
    }

    #[test]
    fn the_hit_when_all_intersections_have_positive_t() {
        let s = sphere();
        let i1 = intersection(1., &s);
        let i2 = intersection(2., &s);
        let xs = intersections(vec![i1, i2]);
        assert_eq!(Some(1.), xs.hit().map(|x| x.t));
    }

    #[test]
    fn the_hit_when_some_intersections_have_negative_t() {
        let s = sphere();
        let i1 = intersection(-1., &s);
        let i2 = intersection(1., &s);
        let xs = intersections(vec![i1, i2]);
        assert_eq!(Some(1.), xs.hit().map(|x| x.t));
    }

    #[test]
    fn the_hit_when_all_intersections_have_negative_t() {
        let s = sphere();
        let i1 = intersection(-2., &s);
        let i2 = intersection(-1., &s);
        let xs = intersections(vec![i1, i2]);
        assert_eq!(None, xs.hit().map(|x| x.t));
    }

    #[test]
    fn the_hit_is_always_the_lowest_nonnegative_intersection() {
        let s = sphere();
        let i1 = intersection(5., &s);
        let i2 = intersection(7., &s);
        let i3 = intersection(-3., &s);
        let i4 = intersection(2., &s);
        let xs = intersections(vec![i1, i2, i3, i4]);
        assert_eq!(Some(2.), xs.hit().map(|x| x.t));
    }

    #[test]
    fn the_hit_should_offset_the_point() {
        let r = ray(point(0, 0, -5), Tuple::Z_UNITV);

        let mut shape = sphere();
        shape.set_transform(translation(0, 0, 1));

        let ix = intersection(5., &shape);
        let ixs = intersections(vec![ix]);
        let comps = ixs[0].prepare_computations(&r, &ixs);

        assert!(comps.over_point.z < -Intersection::EPSILON / 2.);
    }

    #[test]
    fn the_under_point_is_offset_below_the_surface() {
        let r = ray(point(0, 0, -5), Tuple::Z_UNITV);

        let mut shape = sphere();
        shape.set_transform(translation(0, 0, 1));

        let ix = intersection(5., &shape);
        let ixs = intersections(vec![ix]);
        let comps = ixs[0].prepare_computations(&r, &ixs);

        assert!(comps.under_point.z > Intersection::EPSILON / 2.);
        assert!(comps.point.z < comps.under_point.z);
    }

    #[test]
    fn precomputing_the_reflection_vector() {
        let shape = plane();
        let r = ray(point(0, 1, -1), vec(0, -(2f64.sqrt() / 2.), 2f64.sqrt() / 2.));
        let ix = intersection(2f64.sqrt(), &shape);
        let ixs = intersections(vec![ix]);
        let comps = ixs[0].prepare_computations(&r, &ixs);

        assert_eq!(comps.reflectv, vec(0, 2f64.sqrt() / 2., 2f64.sqrt() / 2.));
    }

    #[test]
    fn finding_n1_and_n2_at_various_intersections() {
        let mut a = glass_sphere();
        a.set_transform(scaling(2, 2, 2));
        a.material_mut().refractive_index = 1.5;

        let mut b = glass_sphere();
        b.set_transform(translation(0, 0, -0.25));
        b.material_mut().refractive_index = 2.0;

        let mut c = glass_sphere();
        c.set_transform(translation(0, 0, 0.25));
        c.material_mut().refractive_index = 2.5;

        let r = ray(point(0, 0, -4), Tuple::Z_UNITV);
        let ixs = intersections(vec![
            intersection(2., &a),
            intersection(2.75, &b),
            intersection(3.25, &c),
            intersection(4.75, &b),
            intersection(5.25, &c),
            intersection(6., &a)
        ]);

        let test_cases: [(f64, f64); 6] = [
            (1., 1.5),
            (1.5, 2.,),
            (2., 2.5),
            (2.5, 2.5),
            (2.5, 1.5),
            (1.5, 1.)
        ];

        for (i, (n1, n2)) in test_cases.iter().enumerate() {
            let ix = &ixs[i];
            let comps = ix.prepare_computations(&r, &ixs);

            assert_eq!(comps.n1, *n1, "Case {} - n1 not equal: expected {}, got {}", i, *n1, comps.n1);
            assert_eq!(comps.n2, *n2, "Case {} - n2 not equal: expected {}, got {}", i, *n2, comps.n2);
        }
    }

    #[test]
    fn the_schlick_approximation_under_total_internal_reflection() {
        let shape = glass_sphere();
        let r = ray(point(0, 0, 2f64.sqrt() / 2.), Tuple::Y_UNITV);
        let ixs = intersections(vec![
            intersection(2f64.sqrt() / -2., &shape),
            intersection(2f64.sqrt() / 2., &shape),
        ]);
        let comps = ixs[1].prepare_computations(&r, &ixs);
        let reflectance = comps.schlick();

        assert_eq!(reflectance, 1.);
    }

    #[test]
    fn the_schlick_approximation_with_a_perpendicular_viewing_angle() {
        let shape = glass_sphere();
        let r = ray(Tuple::ORIGIN, Tuple::Y_UNITV);
        let ixs = intersections(vec![
            intersection(-1., &shape),
            intersection(1., &shape),
        ]);
        let comps = ixs[1].prepare_computations(&r, &ixs);
        let reflectance = comps.schlick();

        assert_abs_diff_eq!(reflectance, 0.04);
    }

    #[test]
    fn the_shlick_approximation_with_small_angle_and_n2_gt_n1() {
        let shape = glass_sphere();
        let r = ray(point(0, 0.99, -2), Tuple::Z_UNITV);
        let ixs = intersections(vec![intersection(1.8589, &shape)]);
        let comps = ixs[0].prepare_computations(&r, &ixs);
        let reflectance = comps.schlick();

        assert_abs_diff_eq!(reflectance, 0.48873, epsilon = 1e-4);
    }
}
