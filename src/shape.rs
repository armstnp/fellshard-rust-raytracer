use crate::tuple::Tuple;
use crate::matrix::M44;
use crate::material::{material, Material};
use crate::ray::Ray;
use crate::intersection::Intersections;

pub trait Shape : Send + Sync {
    fn type_tag(&self) -> String;
    fn transform(&self) -> &M44;
    fn set_transform(&mut self, transform: M44);
    fn material(&self) -> &Material;
    fn material_mut(&mut self) -> &mut Material;
    fn set_material(&mut self, material: Material);
    fn local_normal_at(&self, local_point: Tuple) -> Tuple;
    fn local_intersect(&self, local_ray: &Ray) -> Intersections;

    fn normal_at(&self, world_point: Tuple) -> Tuple {
        let inv_transform = self.transform().inverse();
        let local_point = &inv_transform * world_point;
        let local_normal = self.local_normal_at(local_point);
        let mut world_normal = inv_transform.transpose() * local_normal;
        world_normal.w = 0.;
        world_normal.norm()
    }

    fn intersect(&self, ray: &Ray) -> Intersections {
        let local_ray = ray.transform(&self.transform().inverse());
        self.local_intersect(&local_ray)
    }
}

#[derive(Debug, PartialEq)]
pub struct BaseShape {
    transform: M44,
    material: Material
}

impl BaseShape {
    pub fn transform(&self) -> &M44 {
        &self.transform
    }

    pub fn set_transform(&mut self, transform: M44) {
        self.transform = transform;
    }

    pub fn material(&self) -> &Material {
        &self.material
    }

    pub fn material_mut(&mut self) -> &mut Material {
        &mut self.material
    }

    pub fn set_material(&mut self, material: Material) {
        self.material = material;
    }
}

pub fn base_shape() -> BaseShape {
    BaseShape { transform: M44::IDENTITY, material: material() }
}

#[cfg(test)]
mod test {
    use std::f64::consts::PI;
    use crate::shape::*;
    use crate::tuple::{point, vec};
    use crate::matrix::{translation, scaling, rotation_z};
    use crate::ray::ray;
    use crate::intersection::intersections;
    use approx::*;
    use delegate::delegate;
    use serial_test::serial;

    struct TestShape {
        base: BaseShape
    }

    static mut SAVED_RAY: Option<Ray> = None;

    impl Shape for TestShape {
        delegate! {
            to self.base {
                fn transform(&self) -> &M44;
                fn set_transform(&mut self, transform: M44);
                fn material(&self) -> &Material;
                fn material_mut(&mut self) -> &mut Material;
                fn set_material(&mut self, material: Material);
            }
        }

        fn type_tag(&self) -> String { "TestShape".to_string() }

        fn local_normal_at(&self, local_point: Tuple) -> Tuple {
            let mut c = local_point.clone();
            c.w = 0.;
            c
        }

        fn local_intersect(&self, local_ray: &Ray) -> Intersections {
            unsafe {
                SAVED_RAY = Some(*local_ray);
            }
            intersections(vec![])
        }
    }

    fn test_shape() -> TestShape {
        TestShape { base: base_shape() }
    }

    #[test]
    fn the_default_transformation() {
        let s = test_shape();
        assert_eq!(*s.transform(), M44::IDENTITY);
    }

    #[test]
    fn changing_a_shapes_transformation() {
        let mut s = test_shape();
        let t = translation(2, 3, 4);
        s.set_transform(t.clone());
        assert_eq!(*s.transform(), t);
    }

    #[test]
    fn a_shape_has_a_default_material() {
        let s = test_shape();
        assert_eq!(s.material(), &material());
    }

    #[test]
    fn a_shape_may_be_assigned_a_material() {
        let mut s = test_shape();
        let mut m = material();
        m.ambient = 1.;
        s.set_material(m.clone());
        assert_eq!(&m, s.material());
    }

    #[test]
    fn computing_the_normal_on_a_translated_shape() {
        let mut s = test_shape();
        s.set_transform(translation(0, 1, 0));
        let n = s.normal_at(point(0, 1.70711, -0.70711));
        assert_abs_diff_eq!(&n, &vec(0, 0.70711, -0.70711), epsilon = 1e-5);
    }

    #[test]
    fn computing_the_normal_on_a_transformed_shape() {
        let mut s = test_shape();
        s.set_transform(rotation_z(PI / 5.).scale(1, 0.5, 1));
        let n = s.normal_at(point(0, 2f64.sqrt()/2., 2f64.sqrt()/-2.));
        assert_abs_diff_eq!(&n, &vec(0, 0.97014, -0.24254), epsilon = 1e-5);
    }

    #[test]
    #[serial]
    fn intersecting_a_scaled_shape_with_a_ray() {
        let r = ray(point(0, 0, -5), Tuple::Z_UNITV);
        let mut s = test_shape();
        s.set_transform(scaling(2, 2, 2));

        s.intersect(&r); // Test impl used for its side effects

        unsafe {
            let out_ray = SAVED_RAY.unwrap();
            assert_eq!(out_ray.origin, point(0, 0, -2.5));
            assert_eq!(out_ray.direction, Tuple::Z_UNITV / 2.);
        }
    }

    #[test]
    #[serial]
    fn intersecting_a_translated_shape_with_a_ray() {
        let r = ray(point(0, 0, -5), Tuple::Z_UNITV);
        let mut s = test_shape();
        s.set_transform(translation(5, 0, 0));

        s.intersect(&r); // Test impl used for its side effects

        unsafe {
            let out_ray = SAVED_RAY.unwrap();
            assert_eq!(out_ray.origin, point(-5, 0, -5));
            assert_eq!(out_ray.direction, Tuple::Z_UNITV);
        }
    }
}
