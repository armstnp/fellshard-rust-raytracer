use crate::tuple::Tuple;
use crate::matrix::M44;
use crate::material::Material;
use crate::ray::Ray;
use crate::intersection::{intersection, intersections, Intersections};
use crate::shape::{base_shape, Shape, BaseShape};
use delegate::delegate;

#[derive(Debug)]
pub struct Plane {
    base: BaseShape
}

impl Plane {
    const EPSILON: f64 = 1e-7;
}

impl Shape for Plane {
    delegate! {
        to self.base {
            fn transform(&self) -> &M44;
            fn set_transform(&mut self, transform: M44);
            fn material(&self) -> &Material;
            fn material_mut(&mut self) -> &mut Material;
            fn set_material(&mut self, material: Material);
        }
    }

    fn type_tag(&self) -> String { "Plane".to_string() }

    fn local_normal_at(&self, _local_point: Tuple) -> Tuple {
        Tuple::Y_UNITV
    }

    fn local_intersect(&self, local_ray: &Ray) -> Intersections {
        let ix_vec =
            if local_ray.direction.y.abs() < Self::EPSILON {
                vec![]
            } else {
                let t = -local_ray.origin.y / local_ray.direction.y;
                vec![intersection(t, self)]
            };

        intersections(ix_vec)
    }
}

pub fn plane() -> Plane { Plane { base: base_shape() } }

#[cfg(test)]
mod test {
    use crate::plane::*;
    use crate::tuple::point;
    use crate::ray::ray;

    #[test]
    fn the_normal_of_a_plane_is_constant_everywhere() {
        let p = plane();
        let n1 = p.local_normal_at(Tuple::ORIGIN);
        let n2 = p.local_normal_at(point(10, 0, -10));
        let n3 = p.local_normal_at(point(-5, 0, 150));
        assert_eq!(n1, Tuple::Y_UNITV);
        assert_eq!(n2, Tuple::Y_UNITV);
        assert_eq!(n3, Tuple::Y_UNITV);
    }

    #[test]
    fn intersect_with_a_ray_parallel_to_the_plane() {
        let p = plane();
        let r = ray(point(0, 10, 0), Tuple::Z_UNITV);
        let ixs = p.local_intersect(&r);
        assert!(ixs.is_empty())
    }

    #[test]
    fn intersect_with_a_coplanar_ray() {
        let p = plane();
        let r = ray(Tuple::ORIGIN, Tuple::Z_UNITV);
        let ixs = p.local_intersect(&r);
        assert!(ixs.is_empty())
    }

    #[test]
    fn a_ray_intersecting_a_plane_from_above() {
        let p = plane();
        let r = ray(point(0, 1, 0), -Tuple::Y_UNITV);
        let ixs = p.local_intersect(&r);
        assert_eq!(ixs.len(), 1);
        assert_eq!(ixs[0].t, 1.);
        assert!(std::ptr::eq(ixs[0].object as *const dyn Shape as *const Plane, &p));
    }

    #[test]
    fn a_ray_intersecting_a_plane_from_below() {
        let p = plane();
        let r = ray(point(0, -1, 0), Tuple::Y_UNITV);
        let ixs = p.local_intersect(&r);
        assert_eq!(ixs.len(), 1);
        assert_eq!(ixs[0].t, 1.);
        assert!(std::ptr::eq(ixs[0].object as *const dyn Shape as *const Plane, &p));
    }
}
