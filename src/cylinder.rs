use crate::tuple::{vec, Tuple};
use crate::matrix::M44;
use crate::material::Material;
use crate::shape::{base_shape, Shape, BaseShape};
use crate::ray::Ray;
use crate::intersection::{intersection, intersections, Intersection, Intersections};
use delegate::delegate;

#[derive(Debug)]
pub struct Cylinder {
    base: BaseShape,
    pub minimum: f64,
    pub maximum: f64,
    pub closed: bool
}

impl Cylinder {
    const EPSILON: f64 = 1e-7;

    fn check_cap(ray: &Ray, t: f64) -> bool {
        let Tuple { x: orig_x, z: orig_z, .. } = ray.origin;
        let Tuple { x: dir_x, z: dir_z, .. } = ray.direction;

        let x = orig_x + t * dir_x;
        let z = orig_z + t * dir_z;

        x * x + z * z <= 1.
    }

    fn intersect_caps<'a>(&'a self, ray: &Ray, ixs: &mut Vec<Intersection<'a>>) {
        let Tuple { y: orig_y, .. } = ray.origin;
        let Tuple { y: dir_y, .. } = ray.direction;

        if !self.closed || dir_y.abs() <= Self::EPSILON {
            return
        }

        let t = (self.minimum - orig_y) / dir_y;
        if Self::check_cap(ray, t) {
            ixs.push(intersection(t, self));
        }

        let t = (self.maximum - orig_y) / dir_y;
        if Self::check_cap(ray, t) {
            ixs.push(intersection(t, self));
        }
    }
}

impl Shape for Cylinder {
    delegate! {
        to self.base {
            fn transform(&self) -> &M44;
            fn set_transform(&mut self, transform: M44);
            fn material(&self) -> &Material;
            fn material_mut(&mut self) -> &mut Material;
            fn set_material(&mut self, material: Material);
        }
    }

    fn type_tag(&self) -> String { "Cylinder".to_string() }

    fn local_normal_at(&self, local_point: Tuple) -> Tuple {
        let Tuple { x, y, z, .. } = local_point;
        let dist = x * x + z * z;

        if dist < 1. {
            if y >= self.maximum - Self::EPSILON {
                return Tuple::Y_UNITV;
            } else if y <= self.minimum + Self::EPSILON {
                return -Tuple::Y_UNITV;
            }
        }

        vec(x, 0, z)
    }

    fn local_intersect(&self, local_ray: &Ray) -> Intersections {
        use std::mem;

        let Tuple { x: orig_x, y: orig_y, z: orig_z, .. } = local_ray.origin;
        let Tuple { x: dir_x, y: dir_y, z: dir_z, .. } = local_ray.direction;
        let mut ixs = vec![];

        let a = dir_x * dir_x + dir_z * dir_z;
        if a.abs() > Self::EPSILON {
            let b = 2. * orig_x * dir_x + 2. * orig_z * dir_z;
            let c = orig_x * orig_x + orig_z * orig_z - 1.;
            let disc = b * b - 4. * a * c;

            if disc < 0. {
                return intersections(vec![]);
            }

            let mut t0 = (- b - disc.sqrt()) / (2. * a);
            let mut t1 = (- b + disc.sqrt()) / (2. * a);
            if t0 > t1 {
                mem::swap(&mut t0, &mut t1);
            }

            let y0 = orig_y + t0 * dir_y;
            if self.minimum < y0 && y0 < self.maximum {
                ixs.push(intersection(t0, self));
            }

            let y1 = orig_y + t1 * dir_y;
            if self.minimum < y1 && y1 < self.maximum {
                ixs.push(intersection(t1, self));
            }
        }

        self.intersect_caps(local_ray, &mut ixs);

        intersections(ixs)
    }
}

pub fn cylinder() -> Cylinder {
    Cylinder {
        base: base_shape(),
        minimum: f64::NEG_INFINITY,
        maximum: f64::INFINITY,
        closed: false
    }
}

#[cfg(test)]
mod test {
    use crate::cylinder::*;
    use crate::tuple::point;
    use crate::ray::ray;
    use approx::*;

    #[test]
    fn the_default_values_for_a_cylinder() {
        let cyl = cylinder();
        assert_eq!(cyl.minimum, f64::NEG_INFINITY);
        assert_eq!(cyl.maximum, f64::INFINITY);
        assert_eq!(cyl.closed, false);
    }

    #[test]
    fn a_ray_misses_a_cylinder() {
        let cyl = cylinder();

        let cases = [
            (point(1, 0, 0) , Tuple::Y_UNITV),
            (Tuple::ORIGIN  , Tuple::Y_UNITV),
            (point(0, 0, -5), vec(1, 1, 1)  )
        ];

        for (i, (origin, direction)) in cases.iter().enumerate() {
            let r = ray(*origin, direction.norm());
            let ixs = cyl.local_intersect(&r);

            assert_eq!(ixs.len(), 0, "Case {} - Expected no intersections, got {}", i, ixs.len());
        }
    }

    #[test]
    fn a_ray_strikes_a_cylinder() {
        let cyl = cylinder();

        let cases = [
            (point(1  , 0, -5), Tuple::Z_UNITV, 5.     , 5.     ),
            (point(0  , 0, -5), Tuple::Z_UNITV, 4.     , 6.     ),
            (point(0.5, 0, -5), vec(0.1, 1, 1), 6.80798, 7.08872)
        ];

        for (i, (origin, direction, t1, t2)) in cases.iter().enumerate() {
            let r = ray(*origin, direction.norm());
            let ixs = cyl.local_intersect(&r);

            assert_eq!(ixs.len(), 2, "Case {} - Expected two intersections, got {}", i, ixs.len());
            assert!(
                abs_diff_eq!(ixs[0].t, t1, epsilon = 1e-5),
                "Case {} - Expected t1 to be {}, got {}", i, *t1, ixs[0].t);
            assert!(
                abs_diff_eq!(ixs[1].t, t2, epsilon = 1e-5),
                "Case {} - Expected t2 to be {}, got {}", i, *t2, ixs[1].t);
        }
    }

    #[test]
    fn normal_vector_on_a_cylinder() {
        let cyl = cylinder();

        let cases = [
            (point( 1,  0,  0),  Tuple::X_UNITV),
            (point( 0,  5, -1), -Tuple::Z_UNITV),
            (point( 0, -2,  1),  Tuple::Z_UNITV),
            (point(-1,  1,  0), -Tuple::X_UNITV)
        ];

        for (i, (p, normal)) in cases.iter().enumerate() {
            let n = cyl.local_normal_at(*p);
            assert!(
                abs_diff_eq!(n, normal),
                "Case {} - Expected normal of {:?}, got {:?}", i, *normal, n);
        }
    }

    #[test]
    fn intersecting_a_constrained_cylinder() {
        let mut cyl = cylinder();
        cyl.minimum = 1.;
        cyl.maximum = 2.;

        let cases = [
            (point(0, 1.5,  0), vec(0.1, 1, 0), 0),
            (point(0, 3  , -5), Tuple::Z_UNITV, 0),
            (point(0, 0  , -5), Tuple::Z_UNITV, 0),
            (point(0, 2  , -5), Tuple::Z_UNITV, 0),
            (point(0, 1  , -5), Tuple::Z_UNITV, 0),
            (point(0, 1.5, -2), Tuple::Z_UNITV, 2)
        ];

        for (i, (p, direction, count)) in cases.iter().enumerate() {
            let r = ray(*p, direction.norm());
            let ixs = cyl.local_intersect(&r);

            assert_eq!(
                ixs.len(), *count,
                "Case {} - expected {} intersections, got {}", i, *count, ixs.len());
        }
    }

    #[test]
    fn intersecting_the_caps_of_a_closed_cylinder() {
        let mut cyl = cylinder();
        cyl.minimum = 1.;
        cyl.maximum = 2.;
        cyl.closed = true;

        let cases = [
            (point(0,  3,  0), -Tuple::Y_UNITV, 2),
            (point(0,  3, -2), vec(0, -1, 2),   2),
            (point(0,  4, -2), vec(0, -1, 1),   2),
            (point(0,  0, -2), vec(0, 1, 2),    2),
            (point(0, -1, -2), vec(0, 1, 1),    2)
        ];

        for (i, (p, direction, count)) in cases.iter().enumerate() {
            let r = ray(*p, direction.norm());
            let ixs = cyl.local_intersect(&r);

            assert_eq!(
                ixs.len(), *count,
                "Case {} - expected {} intersections, got {}", i, *count, ixs.len());
        }
    }

    #[test]
    fn the_normal_vector_on_a_cylinders_end_caps() {
        let mut cyl = cylinder();
        cyl.minimum = 1.;
        cyl.maximum = 2.;
        cyl.closed = true;

        let cases = [
            (point(0  , 1, 0  ), -Tuple::Y_UNITV),
            (point(0.5, 1, 0  ), -Tuple::Y_UNITV),
            (point(0  , 1, 0.5), -Tuple::Y_UNITV),
            (point(0  , 2, 0  ),  Tuple::Y_UNITV),
            (point(0.5, 2, 0  ),  Tuple::Y_UNITV),
            (point(0  , 2, 0.5),  Tuple::Y_UNITV),
        ];

        for (i, (p, normal)) in cases.iter().enumerate() {
            let n = cyl.local_normal_at(*p);

            assert!(
                abs_diff_eq!(n, normal),
                "Case {} - Expected normal {:?}, got {:?}", i, *normal, n);
        }
    }
}
