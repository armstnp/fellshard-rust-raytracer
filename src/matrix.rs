use std::ops::Mul;
use std::fmt;
use crate::tuple::Tuple;
use approx::*;

macro_rules! matrix_index {
    ($type:ty; $width:expr; $height:expr) => (impl std::ops::Index<usize> for $type {
        type Output = [f64];

        fn index(&self, row: usize) -> &Self::Output {
            assert!(row < $height);

            &self.0[(row * $width)..((row + 1) * $width)]
        }
    })
}

#[derive(Debug, PartialEq, Clone)]
pub struct M22([f64; 4]);
matrix_index!(M22; 2; 2);

impl M22 {
    pub fn determinant(&self) -> f64 {
        let x = &self.0;
        x[0] * x[3] - x[1] * x[2]
    }
}

impl fmt::Display for M22 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let x = self.0;
        write!(f, "Mtx [{:>7.4} {:>7.4}\n     {:>7.4} {:>7.4}]", x[0], x[1], x[2], x[3])
    }
}

fn m22(
    x00: impl Into<f64>, x01: impl Into<f64>,
    x10: impl Into<f64>, x11: impl Into<f64>) -> M22 {
    M22([x00.into(), x01.into(), x10.into(), x11.into()])
}

#[derive(Debug, PartialEq, Clone)]
pub struct M33([f64; 9]);
matrix_index!(M33; 3; 3);

impl M33 {
    pub fn submatrix(&self, elide_row: usize, elide_col: usize) -> M22 {
        let mut sub: [f64; 4] = Default::default();

        let mut target_index = 0;
        for row in 0..=2 {
            if row == elide_row { continue; }
            for col in 0..=2 {
                if col == elide_col { continue; }

                sub[target_index] = self.0[row * 3 + col];
                target_index += 1;
            }
        }

        M22(sub)
    }

    pub fn minor(&self, elide_row: usize, elide_col: usize) -> f64 {
        self.submatrix(elide_row, elide_col).determinant()
    }

    pub fn cofactor(&self, elide_row: usize, elide_col: usize) -> f64 {
        let sign = if (elide_row + elide_col) % 2 == 0 { 1. } else { -1. };
        self.minor(elide_row, elide_col) * sign
    }

    pub fn determinant(&self) -> f64 {
        let x = &self.0;
        let mut det = 0.;

        for col in 0..=2 {
            det += x[col] * self.cofactor(0, col);
        }

        det
    }
}

impl fmt::Display for M33 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let x = self.0;
        write!(f, "Mtx [{:>7.4} {:>7.4} {:>7.4}\n     {:>7.4} {:>7.4} {:>7.4}\n     {:>7.4} {:>7.4} {:>7.4}]",
               x[0], x[1], x[2], x[3], x[4], x[5], x[6], x[7], x[8])
    }
}

fn m33(
    x00: impl Into<f64>, x01: impl Into<f64>, x02: impl Into<f64>,
    x10: impl Into<f64>, x11: impl Into<f64>, x12: impl Into<f64>,
    x20: impl Into<f64>, x21: impl Into<f64>, x22: impl Into<f64>) -> M33 {
    M33([x00.into(), x01.into(), x02.into(),
         x10.into(), x11.into(), x12.into(),
         x20.into(), x21.into(), x22.into()])
}

#[derive(Debug, PartialEq, Clone)]
pub struct M44([f64; 16]);
matrix_index!(M44; 4; 4);

impl approx::AbsDiffEq for M44 {
    type Epsilon = f64;

    fn default_epsilon() -> Self::Epsilon { 1e-5 }

    fn abs_diff_eq(&self, other: &Self, epsilon: Self::Epsilon) -> bool {
        self.0
            .iter()
            .zip(&other.0)
            .all(|(x, y)| x.abs_diff_eq(y, epsilon))
    }
}

impl Mul for M44 {
    type Output = M44;

    fn mul(self, other: Self) -> Self::Output {
        &self * &other
    }
}

impl Mul<&M44> for M44 {
    type Output = M44;

    fn mul(self, other: &Self) -> Self::Output {
        &self * other
    }
}

impl Mul<M44> for &M44 {
    type Output = M44;

    fn mul(self, other: M44) -> Self::Output {
        self * &other
    }
}

impl Mul for &M44 {
    type Output = M44;

    fn mul(self, other: &M44) -> Self::Output {
        let mut m: [f64; 16] = Default::default();
        for row in 0..4 {
            for col in 0..4 {
                m[row * 4 + col] =
                    self[row][0] * other[0][col] +
                    self[row][1] * other[1][col] +
                    self[row][2] * other[2][col] +
                    self[row][3] * other[3][col];
            }
        }

        M44(m)
    }
}

impl Mul<Tuple> for M44 {
    type Output = Tuple;

    fn mul(self, other: Tuple) -> Self::Output {
        &self * &other
    }
}

impl Mul<&Tuple> for M44 {
    type Output = Tuple;

    fn mul(self, other: &Tuple) -> Self::Output {
        &self * other
    }
}

impl Mul<Tuple> for &M44 {
    type Output = Tuple;

    fn mul(self, other: Tuple) -> Self::Output {
        self * &other
    }
}

impl Mul<&Tuple> for &M44 {
    type Output = Tuple;

    fn mul(self, other: &Tuple) -> Self::Output {
        let mut m: [f64; 4] = Default::default();
        for row in 0..4 {
            m[row] =
                self[row][0] * other.x +
                self[row][1] * other.y +
                self[row][2] * other.z +
                self[row][3] * other.w;
        }

        Tuple::from(m)
    }
}

impl fmt::Display for M44 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let x = self.0;
        write!(f, "Mtx [{:>7.4} {:>7.4} {:>7.4} {:>7.4}\n     {:>7.4} {:>7.4} {:>7.4} {:>7.4}\n     {:>7.4} {:>7.4} {:>7.4} {:>7.4}\n     {:>7.4} {:>7.4} {:>7.4} {:>7.4}]",
               x[0], x[1], x[2], x[3], x[4], x[5], x[6], x[7], x[8],
               x[9], x[10], x[11], x[12], x[13], x[14], x[15])
    }
}

impl M44 {
    pub const IDENTITY: M44 = M44([1., 0., 0., 0.,
                                   0., 1., 0., 0.,
                                   0., 0., 1., 0.,
                                   0., 0., 0., 1.]);

    pub fn transpose(&self) -> M44 {
        let x = &self.0;

        M44([x[0], x[4],  x[8], x[12],
             x[1], x[5],  x[9], x[13],
             x[2], x[6], x[10], x[14],
             x[3], x[7], x[11], x[15]])
    }

    pub fn submatrix(&self, elide_row: usize, elide_col: usize) -> M33 {
        let mut sub: [f64; 9] = Default::default();

        let mut target_index = 0;
        for row in 0..=3 {
            if row == elide_row { continue; }
            for col in 0..=3 {
                if col == elide_col { continue; }

                sub[target_index] = self.0[row * 4 + col];
                target_index += 1;
            }
        }

        M33(sub)
    }

    pub fn minor(&self, elide_row: usize, elide_col: usize) -> f64 {
        self.submatrix(elide_row, elide_col).determinant()
    }

    pub fn cofactor(&self, elide_row: usize, elide_col: usize) -> f64 {
        let sign = if (elide_row + elide_col) % 2 == 0 { 1. } else { -1. };
        self.minor(elide_row, elide_col) * sign
    }

    pub fn determinant(&self) -> f64 {
        let x = &self.0;
        let mut det = 0.;

        for col in 0..=3 {
            det += x[col] * self.cofactor(0, col);
        }

        det
    }

    pub fn is_invertible(&self) -> bool {
        abs_diff_ne!(self.determinant(), 0.)
    }

    pub fn inverse(&self) -> M44 {
        assert!(self.is_invertible());

        let mut inv: [f64; 16] = Default::default();
        let det = self.determinant();

        for row in 0..=3 {
            for col in 0..=3 {
                inv[col * 4 + row] = self.cofactor(row, col) / det;
            }
        }

        M44(inv)
    }

    pub fn translate(&self, x: impl Into<f64>, y: impl Into<f64>, z: impl Into<f64>) -> M44 {
        translation(x, y, z) * self
    }

    pub fn scale(&self, x: impl Into<f64>, y: impl Into<f64>, z: impl Into<f64>) -> M44 {
        scaling(x, y, z) * self
    }

    pub fn rotate_x(&self, radians: impl Into<f64>) -> M44 {
        rotation_x(radians) * self
    }

    pub fn rotate_y(&self, radians: impl Into<f64>) -> M44 {
        rotation_y(radians) * self
    }

    pub fn rotate_z(&self, radians: impl Into<f64>) -> M44 {
        rotation_z(radians) * self
    }

    pub fn shear(&self,
                 x_y: impl Into<f64>, x_z: impl Into<f64>,
                 y_x: impl Into<f64>, y_z: impl Into<f64>,
                 z_x: impl Into<f64>, z_y: impl Into<f64>) -> M44 {
        shearing(x_y, x_z, y_x, y_z, z_x, z_y) * self
    }
}

pub fn m44(
    x00: impl Into<f64>, x01: impl Into<f64>, x02: impl Into<f64>, x03: impl Into<f64>,
    x10: impl Into<f64>, x11: impl Into<f64>, x12: impl Into<f64>, x13: impl Into<f64>,
    x20: impl Into<f64>, x21: impl Into<f64>, x22: impl Into<f64>, x23: impl Into<f64>,
    x30: impl Into<f64>, x31: impl Into<f64>, x32: impl Into<f64>, x33: impl Into<f64>) -> M44 {
    M44([x00.into(), x01.into(), x02.into(), x03.into(),
         x10.into(), x11.into(), x12.into(), x13.into(),
         x20.into(), x21.into(), x22.into(), x23.into(),
         x30.into(), x31.into(), x32.into(), x33.into()])
}

pub fn translation(x: impl Into<f64>, y: impl Into<f64>, z: impl Into<f64>) -> M44 {
    m44(1, 0, 0, x.into(),
        0, 1, 0, y.into(),
        0, 0, 1, z.into(),
        0, 0, 0,        1)
}

pub fn scaling(x: impl Into<f64>, y: impl Into<f64>, z: impl Into<f64>) -> M44 {
    m44(x.into(),        0,        0, 0,
               0, y.into(),        0, 0,
               0,        0, z.into(), 0,
               0,        0,        0, 1)
}

pub fn rotation_x(radians: impl Into<f64>) -> M44 {
    let r_f = radians.into();
    let s = r_f.sin();
    let c = r_f.cos();
    m44(1, 0,  0, 0,
        0, c, -s, 0,
        0, s,  c, 0,
        0, 0,  0, 1)
}

pub fn rotation_y(radians: impl Into<f64>) -> M44 {
    let r_f = radians.into();
    let s = r_f.sin();
    let c = r_f.cos();
    m44( c, 0, s, 0,
         0, 1, 0, 0,
        -s, 0, c, 0,
         0, 0, 0, 1)
}

pub fn rotation_z(radians: impl Into<f64>) -> M44 {
    let r_f = radians.into();
    let s = r_f.sin();
    let c = r_f.cos();
    m44(c, -s, 0, 0,
        s,  c, 0, 0,
        0,  0, 1, 0,
        0,  0, 0, 1)
}

pub fn shearing(x_y: impl Into<f64>, x_z: impl Into<f64>,
                y_x: impl Into<f64>, y_z: impl Into<f64>,
                z_x: impl Into<f64>, z_y: impl Into<f64>) -> M44 {
    m44(         1, x_y.into(), x_z.into(), 0,
        y_x.into(),          1, y_z.into(), 0,
        z_x.into(), z_y.into(),          1, 0,
                 0,          0,          0, 1)
}

pub fn view_transform(from: Tuple, to: Tuple, up: Tuple) -> M44 {
    let forward = (to - from).norm();
    let left = forward.cross(up.norm());
    let true_up = left.cross(forward);
    let orientation = m44(left.x,     left.y,     left.z,     0,
                          true_up.x,  true_up.y,  true_up.z,  0,
                          -forward.x, -forward.y, -forward.z, 0,
                          0,          0,          0,          1);
    orientation * translation(-from.x, -from.y, -from.z)
}

#[cfg(test)]
mod test {
    use crate::matrix::*;
    use crate::tuple::{point, vec};
    use std::f64::consts::PI;

    #[test]
    fn constructing_and_inspecting_a_4x4_matrix() {
        let m = m44(   1,    2,    3,    4,
                     5.5,  6.5,  7.5,  8.5,
                       9,   10,   11,   12,
                    13.5, 14.5, 15.5, 16.5);

        assert!(abs_diff_eq!(m[0][0], 1.));
        assert!(abs_diff_eq!(m[0][3], 4.));
        assert!(abs_diff_eq!(m[1][0], 5.5));
        assert!(abs_diff_eq!(m[1][2], 7.5));
        assert!(abs_diff_eq!(m[2][2], 11.));
        assert!(abs_diff_eq!(m[3][0], 13.5));
        assert!(abs_diff_eq!(m[3][2], 15.5));
    }

    #[test]
    fn a_2x2_matrix_ought_to_be_representable() {
        let m = m22(-3,  5,
                     1, -2);

        assert!(abs_diff_eq!(m[0][0], -3.));
        assert!(abs_diff_eq!(m[0][1],  5.));
        assert!(abs_diff_eq!(m[1][0],  1.));
        assert!(abs_diff_eq!(m[1][1], -2.));
    }

    #[test]
    fn a_3x3_matrix_ought_to_be_representable() {
        let m = m33(-3,  5,  0,
                     1, -2, -7,
                     0,  1,  1);

        assert!(abs_diff_eq!(m[0][0], -3.));
        assert!(abs_diff_eq!(m[1][1], -2.));
        assert!(abs_diff_eq!(m[2][2],  1.));
    }

    #[test]
    fn matrix_equality_with_identical_matrices() {
        let a = m44(1, 2, 3, 4,
                    5, 6, 7, 8,
                    9, 8, 7, 6,
                    5, 4, 3, 2);
        let b = m44(1, 2, 3, 4,
                    5, 6, 7, 8,
                    9, 8, 7, 6,
                    5, 4, 3, 2);

        assert_eq!(a, b);
    }

    #[test]
    fn matrix_equality_with_different_matrices() {
        let a = m44(1, 2, 3, 4,
                    5, 6, 7, 8,
                    9, 8, 7, 6,
                    5, 4, 3, 2);
        let b = m44(2, 3, 4, 5,
                    6, 7, 8, 9,
                    8, 7, 6, 5,
                    4, 3, 2, 1);

        assert!(a != b);
    }

    #[test]
    fn multiplying_two_matrices() {
        let a = m44(1, 2, 3, 4,
                    5, 6, 7, 8,
                    9, 8, 7, 6,
                    5, 4, 3, 2);
        let b = m44(-2, 1, 2,  3,
                     3, 2, 1, -1,
                     4, 3, 6,  5,
                     1, 2, 7,  8);

        assert_eq!(
            a * b,
            m44(20, 22,  50,  48,
                44, 54, 114, 108,
                40, 58, 110, 102,
                16, 26,  46,  42));
    }

    #[test]
    fn a_matrix_multiplied_by_a_tuple() {
        let m = m44(1, 2, 3, 4,
                    2, 4, 4, 2,
                    8, 6, 4, 1,
                    0, 0, 0, 1);
        let t = Tuple::from([1., 2., 3., 1.]);

        assert_eq!(m * t, Tuple::from([18., 24., 33., 1.]));
    }

    #[test]
    fn multiplying_a_matrix_by_the_identity_matrix() {
        let m = m44(0, 1,  2,  4,
                    1, 2,  4,  8,
                    2, 4,  8, 16,
                    4, 8, 16, 32);
        assert_eq!(&m * M44::IDENTITY, m);
    }

    #[test]
    fn multiplying_the_identity_matrix_by_a_tuple() {
        let t = Tuple::from([1., 2., 3., 4.]);
        assert_eq!(M44::IDENTITY * t, t);
    }

    #[test]
    fn transposing_a_matrix() {
        let m = m44(0, 9, 3, 0,
                    9, 8, 0, 8,
                    1, 8, 5, 3,
                    0, 0, 5, 8);
        let mtrans = m44(0, 9, 1, 0,
                         9, 8, 8, 0,
                         3, 0, 5, 5,
                         0, 8, 3, 8);
        assert_eq!(m.transpose(), mtrans);
    }

    #[test]
    fn transposing_the_identity_matrix() {
        assert_eq!(M44::IDENTITY.transpose(), M44::IDENTITY);
    }

    #[test]
    fn calculating_the_determinant_of_a_2x2_matrix() {
        assert!(abs_diff_eq!(m22(1, 5, -3, 2).determinant(), 17.));
    }

    #[test]
    fn a_submatrix_of_a_3x3_matrix_is_a_2x2_matrix() {
        let m = m33( 1, 5,  0,
                    -3, 2,  7,
                     0, 6, -3);
        assert_eq!(m.submatrix(0, 2), m22(-3, 2, 0, 6));
    }

    #[test]
    fn a_submatrix_of_a_4x4_matrix_is_a_3x3_matrix() {
        let m = m44( -6, 1,  1, 6,
                     -8, 5,  8, 6,
                     -1, 0,  8, 2,
                     -7, 1, -1, 1);
        let m_sub = m33(-6,  1, 6,
                        -8,  8, 6,
                        -7, -1, 1);
        assert_eq!(m.submatrix(2, 1), m_sub);
    }

    #[test]
    fn calculating_a_minor_of_a_3x3_matrix() {
        let a = m33(3,  5,  0,
                    2, -1, -7,
                    6, -1,  5);
        let b = a.submatrix(1, 0);
        assert!(abs_diff_eq!(b.determinant(), a.minor(1, 0)));
    }

    #[test]
    fn calculating_a_cofactor_of_a_3x3_matrix() {
        let m = m33(3,  5,  0,
                    2, -1, -7,
                    6, -1,  5);
        assert!(abs_diff_eq!(m.minor(0, 0), -12.));
        assert!(abs_diff_eq!(m.cofactor(0, 0), -12.));
        assert!(abs_diff_eq!(m.minor(1, 0), 25.));
        assert!(abs_diff_eq!(m.cofactor(1, 0), -25.));
    }

    #[test]
    fn calculating_the_determinant_of_a_3x3_matrix() {
        let m = m33( 1, 2,  6,
                    -5, 8, -4,
                     2, 6,  4);
        assert!(abs_diff_eq!(m.cofactor(0, 0), 56.));
        assert!(abs_diff_eq!(m.cofactor(0, 1), 12.));
        assert!(abs_diff_eq!(m.cofactor(0, 2), -46.));
        assert!(abs_diff_eq!(m.determinant(), -196.));
    }

    #[test]
    fn calculating_the_determinant_of_a_4x4_matrix() {
        let m = m44(-2, -8,  3,  5,
                    -3,  1,  7,  3,
                     1,  2, -9,  6,
                    -6,  7,  7, -9);
        assert!(abs_diff_eq!(m.cofactor(0, 0), 690.));
        assert!(abs_diff_eq!(m.cofactor(0, 1), 447.));
        assert!(abs_diff_eq!(m.cofactor(0, 2), 210.));
        assert!(abs_diff_eq!(m.cofactor(0, 3), 51.));
        assert!(abs_diff_eq!(m.determinant(), -4071.));
    }

    #[test]
    fn testing_an_invertible_matrix_for_invertibility() {
        let m = m44(6,  4, 4,  4,
                    5,  5, 7,  6,
                    4, -9, 3, -7,
                    9,  1, 7, -6);
        assert!(abs_diff_eq!(m.determinant(), -2120.));
        assert!(m.is_invertible());
    }

    #[test]
    fn testing_a_noninvertible_matrix_for_invertibility() {
        let m = m44(-4,  2, -2,  3,
                     9,  6,  2,  6,
                     0, -5,  1, -5,
                     0,  0,  0,  0);
        assert!(abs_diff_eq!(m.determinant(), 0.));
        assert!(!m.is_invertible());
    }

    #[test]
    fn calculating_the_inverse_of_a_matrix() {
        let a = m44(-5,  2,  6, -8,
                     1, -5,  1,  8,
                     7,  7, -6, -7,
                     1, -3,  7,  4);
        let b = a.inverse();
        let inv = m44( 0.21805,  0.45113,  0.24060, -0.04511,
                      -0.80827, -1.45677, -0.44361,  0.52068,
                      -0.07895, -0.22368, -0.05263,  0.19737,
                      -0.52256, -0.81391, -0.30075,  0.30639);

        assert_abs_diff_eq!(a.determinant(), 532.);
        assert_abs_diff_eq!(a.cofactor(2, 3), -160.);
        assert_abs_diff_eq!(b.0[14], -160./532.);
        assert_abs_diff_eq!(a.cofactor(3, 2), 105.);
        assert_abs_diff_eq!(b.0[11], 105./532.);
        assert_abs_diff_eq!(b.0[..], inv.0[..], epsilon = 1e-5);
    }

    #[test]
    fn calculating_the_inverse_of_another_matrix() {
        let a = m44( 8, -5,  9,  2,
                     7,  5,  6,  1,
                    -6,  0,  9,  6,
                    -3, -0, -9, -4);
        let inv = m44(-0.15385, -0.15385, -0.28205, -0.53846,
                      -0.07692,  0.12308,  0.02564,  0.03077,
                       0.35897,  0.35897,  0.43590,  0.92308,
                      -0.69231, -0.69231, -0.76923, -1.92308);

        assert_abs_diff_eq!(a.inverse().0[..], inv.0[..], epsilon = 1e-5);
    }

    #[test]
    fn calculating_the_inverse_of_a_third_matrix() {
        let a = m44( 9,  3,  0,  9,
                    -5, -2, -6, -3,
                    -4,  9,  6,  4,
                    -7,  6,  6,  2);
        let inv = m44(-0.04074, -0.07778,  0.14444, -0.22222,
                      -0.07778,  0.03333,  0.36667, -0.33333,
                      -0.02901, -0.14630, -0.10926,  0.12963,
                       0.17778,  0.06667, -0.26667,  0.33333);

        assert_abs_diff_eq!(a.inverse().0[..], inv.0[..], epsilon = 1e-5);
    }

    #[test]
    fn multiplying_a_product_by_its_inverse() {
        let a = m44( 3, -9,  7,  3,
                     3, -8,  2, -9,
                    -4,  4,  4,  1,
                    -6,  5, -1,  1);
        let b = m44(8,  2, 2, 2,
                    3, -1, 7, 0,
                    7,  0, 5, 4,
                    6, -2, 0, 5);
        let c = &a * &b;
        assert_abs_diff_eq!((c * b.inverse()).0[..], a.0[..], epsilon = 1e-5);
    }

    #[test]
    fn multiplying_by_a_translation_matrix() {
        let transformed = translation(5, -3, 2) * point(-3, 4, 5);
        assert_eq!(transformed, point(2, 1, 7));
    }

    #[test]
    fn multiplying_by_the_inverse_of_a_translation_matrix() {
        let transformed = translation(5, -3, 2).inverse() * point(-3, 4, 5);
        assert_eq!(transformed, point(-8, 7, 3));
    }

    #[test]
    fn translation_does_not_affect_vectors() {
        let v = vec(-3, 4, 5);
        let transformed = translation(5, -3, 2) * v;
        assert_eq!(transformed, v);
    }

    #[test]
    fn a_scaling_matrix_applied_to_a_point() {
        let transformed = scaling(2, 3, 4) * point(-4, 6, 8);
        assert_eq!(transformed, point(-8, 18, 32));
    }

    #[test]
    fn a_scaling_matrix_applied_to_a_vector() {
        let transformed = scaling(2, 3, 4) * vec(-4, 6, 8);
        assert_eq!(transformed, vec(-8, 18, 32));
    }

    #[test]
    fn multiplying_by_the_inverse_of_a_scaling_matrix() {
        let transformed = scaling(2, 3, 4).inverse() * vec(-4, 6, 8);
        assert_eq!(transformed, vec(-2, 2, 2));
    }

    #[test]
    fn reflection_is_scaling_by_a_negative_value() {
        let transformed = scaling(-1, 1, 1).inverse() * point(2, 3, 4);
        assert_eq!(transformed, point(-2, 3, 4));
    }

    #[test]
    fn rotating_a_point_around_the_x_axis() {
        let p = Tuple::ORIGIN + Tuple::Y_UNITV;
        let half_quarter = rotation_x(PI / 4.);
        let full_quarter = rotation_x(PI / 2.);

        assert_abs_diff_eq!(half_quarter * p, point(0, 2f64.sqrt()/2., 2f64.sqrt()/2.));
        assert_abs_diff_eq!(full_quarter * p, Tuple::ORIGIN + Tuple::Z_UNITV);
    }

    #[test]
    fn the_inverse_of_an_x_rotation_rotates_in_the_opposite_direction() {
        let p = Tuple::ORIGIN + Tuple::Y_UNITV;
        let half_quarter = rotation_x(PI / 4.);
        let transformed = half_quarter.inverse() * p;
        assert_abs_diff_eq!(transformed, point(0, 2f64.sqrt()/2., 2f64.sqrt()/-2.));
    }

    #[test]
    fn rotating_a_point_around_the_y_axis() {
        let p = Tuple::ORIGIN + Tuple::Z_UNITV;
        let half_quarter = rotation_y(PI / 4.);
        let full_quarter = rotation_y(PI / 2.);

        assert_abs_diff_eq!(half_quarter * p, point(2f64.sqrt()/2., 0, 2f64.sqrt()/2.));
        assert_abs_diff_eq!(full_quarter * p, Tuple::ORIGIN + Tuple::X_UNITV);
    }

    #[test]
    fn rotating_a_point_around_the_z_axis() {
        let p = Tuple::ORIGIN + Tuple::Y_UNITV;
        let half_quarter = rotation_z(PI / 4.);
        let full_quarter = rotation_z(PI / 2.);

        assert_abs_diff_eq!(half_quarter * p, point(2f64.sqrt()/-2., 2f64.sqrt()/2., 0));
        assert_abs_diff_eq!(full_quarter * p, Tuple::ORIGIN - Tuple::X_UNITV);
    }

    #[test]
    fn a_shearing_transformation_moves_x_in_proportion_to_y() {
        let transformed = shearing(1, 0, 0, 0, 0, 0) * point(2, 3, 4);
        assert_eq!(transformed, point(5, 3, 4));
    }

    #[test]
    fn a_shearing_transformation_moves_x_in_proportion_to_z() {
        let transformed = shearing(0, 1, 0, 0, 0, 0) * point(2, 3, 4);
        assert_eq!(transformed, point(6, 3, 4));
    }

    #[test]
    fn a_shearing_transformation_moves_y_in_proportion_to_x() {
        let transformed = shearing(0, 0, 1, 0, 0, 0) * point(2, 3, 4);
        assert_eq!(transformed, point(2, 5, 4));
    }

    #[test]
    fn a_shearing_transformation_moves_y_in_proportion_to_z() {
        let transformed = shearing(0, 0, 0, 1, 0, 0) * point(2, 3, 4);
        assert_eq!(transformed, point(2, 7, 4));
    }

    #[test]
    fn a_shearing_transformation_moves_z_in_proportion_to_x() {
        let transformed = shearing(0, 0, 0, 0, 1, 0) * point(2, 3, 4);
        assert_eq!(transformed, point(2, 3, 6));
    }

    #[test]
    fn a_shearing_transformation_moves_z_in_proportion_to_y() {
        let transformed = shearing(0, 0, 0, 0, 0, 1) * point(2, 3, 4);
        assert_eq!(transformed, point(2, 3, 7));
    }

    #[test]
    fn individual_transformations_are_applied_in_sequence() {
        let p1 = point(1, 0, 1);
        let a = rotation_x(PI/2.);
        let b = scaling(5, 5, 5);
        let c = translation(10, 5, 7);

        let p2 = a * p1;
        assert_abs_diff_eq!(p2, point(1, -1, 0));

        let p3 = b * p2;
        assert_abs_diff_eq!(p3, point(5, -5, 0));

        let p4 = c * p3;
        assert_abs_diff_eq!(p4, point(15, 0, 7));
    }

    #[test]
    fn chained_transformations_must_be_applied_in_reverse_order() {
        let p = point(1, 0, 1);
        let a = rotation_x(PI/2.);
        let b = scaling(5, 5, 5);
        let c = translation(10, 5, 7);
        let transform = c * b * a;

        assert_abs_diff_eq!(transform * p, point(15, 0, 7));
    }

    // Custom addition hinted at in the book
    #[test]
    fn chained_transformations_can_be_derived_fluently() {
        let p = point(1, 0, 1);
        let transform =
            rotation_x(PI/2.)
            .scale(5, 5, 5)
            .translate(10, 5, 7);

        assert_abs_diff_eq!(transform * p, point(15, 0, 7));
    }

    #[test]
    fn the_transformation_matrix_for_the_default_orientation() {
        let from = Tuple::ORIGIN;
        let to = Tuple::ORIGIN - Tuple::Z_UNITV;
        let up = Tuple::Y_UNITV;
        let t = view_transform(from, to, up);
        assert_abs_diff_eq!(t, M44::IDENTITY);
    }

    #[test]
    fn a_view_transformation_matrix_looking_in_positive_z_direction() {
        let from = Tuple::ORIGIN;
        let to = Tuple::ORIGIN + Tuple::Z_UNITV;
        let up = Tuple::Y_UNITV;
        let t = view_transform(from, to, up);
        assert_abs_diff_eq!(t, scaling(-1, 1, -1));
    }

    #[test]
    fn the_view_transformation_moves_the_world() {
        let from = Tuple::ORIGIN + Tuple::Z_UNITV * 8;
        let to = Tuple::ORIGIN;
        let up = Tuple::Y_UNITV;
        let t = view_transform(from, to, up);
        assert_abs_diff_eq!(t, translation(0, 0, -8));
    }

    #[test]
    fn an_arbitrary_view_transformation() {
        let from = point(1, 3, 2);
        let to = point(4, -2, 8);
        let up = Tuple::X_UNITV + Tuple::Y_UNITV;
        let t = view_transform(from, to, up);
        assert_abs_diff_eq!(t, m44(-0.50709, 0.50709,  0.67612, -2.36643,
                                    0.76772, 0.60609,  0.12122, -2.82843,
                                   -0.35857, 0.59761, -0.71714,        0,
                                          0,       0,        0,        1));
    }
}
