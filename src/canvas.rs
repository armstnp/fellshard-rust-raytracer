use crate::color::Color;

const PPM_RANGE: u8 = 255;
const PPM_RANGE_STR: &str = "255";
const PPM_LINE_LIMIT: usize = 70;

pub struct Canvas {
    px: Vec<Color>,
    w: usize,
    h: usize
}

impl Canvas {
    pub fn width(&self) -> usize {
        self.w
    }

    pub fn height(&self) -> usize {
        self.h
    }

    pub fn in_bounds(&self, x: usize, y: usize) -> bool {
        x < self.w && y < self.h
    }

    fn index(&self, x: usize, y: usize) -> usize {
        assert!(self.in_bounds(x, y));

        y * self.w + x
    }

    pub fn write_px(&mut self, x: usize, y: usize, col: Color) {
        let index = self.index(x, y);
        self.px[index] = col
    }

    pub fn read_px(&self, x: usize, y: usize) -> Color {
        self.px[self.index(x, y)]
    }

    pub fn fill(&mut self, col: Color) {
        for pixel in self.px.iter_mut() {
            *pixel = col
        }
    }

    // Later: convert to Iter structure of PPM lines?
    fn px_to_ppm(&self) -> String {
        use textwrap::fill;

        self.px
            .chunks(self.w)
            .map(|chunk|
                 fill(&chunk.iter()
                            .map(|c|
                                 c.to_ppm(PPM_RANGE)
                                  .iter()
                                  .map(|c| format!("{}", c))
                                  .collect::<Vec<String>>()
                                  .join(" "))
                            .collect::<Vec<String>>()
                            .join(" "),
                      PPM_LINE_LIMIT))
            .collect::<Vec<String>>()
            .join("\n")
    }

    pub fn to_ppm(&self) -> String {
        let dims = format!("{} {}", self.w, self.h);

        let mut ppm: Vec<&str> = Vec::with_capacity(self.h + 3);
        ppm.push("P3");
        ppm.push(&dims);
        ppm.push(PPM_RANGE_STR);

        let ppm_vals = self.px_to_ppm();

        ppm.push(&ppm_vals);

        ppm.join("\n") + "\n"
    }
}

pub fn canvas(width: usize, height: usize) -> Canvas {
    // TODO: Consider checking for overflow size?
    Canvas {
        px: vec![Color::BLACK; width * height],
        w: width,
        h: height,
    }
}

#[cfg(test)]
mod test {
    use crate::canvas::*;
    use crate::color::color;
    use approx::*;

    #[test]
    fn initializes_black_canvas() {
        let c = canvas(10, 20);
        assert_eq!(c.width(), 10);
        assert_eq!(c.height(), 20);
        assert!(c.px.iter().all(|col| abs_diff_eq!(col, &Color::BLACK)));
    }

    #[test]
    fn writes_pixels() {
        let mut c = canvas(10, 20);
        c.write_px(2, 3, Color::RED);
        assert!(abs_diff_eq!(c.read_px(2, 3), Color::RED));
    }

    #[test]
    fn constructs_the_ppm_header() {
        assert!(canvas(5, 3).to_ppm().lines().take(3).collect::<Vec<&str>>().eq(&vec!["P3", "5 3", "255"]))
    }

    #[test]
    fn constructs_the_ppm_pixel_data() {
        let mut c = canvas(5, 3);
        let c1 = Color::RED * 1.5;
        let c2 = Color::GREEN * 0.5;
        let c3 = color(-0.5, 0., 1.);

        c.write_px(0, 0, c1);
        c.write_px(2, 1, c2);
        c.write_px(4, 2, c3);

        let ppm = c.to_ppm();
        let mut lines = ppm.lines();

        // Skip header
        lines.next();
        lines.next();
        lines.next();

        assert_eq!(Some("255 0 0 0 0 0 0 0 0 0 0 0 0 0 0"), lines.next());
        assert_eq!(Some("0 0 0 0 0 0 0 128 0 0 0 0 0 0 0"), lines.next());
        assert_eq!(Some("0 0 0 0 0 0 0 0 0 0 0 0 0 0 255"), lines.next());
    }

    #[test]
    fn splitting_long_lines_in_ppm_files() {
        let mut c = canvas(10, 2);
        c.fill(color(1., 0.8, 0.6));

        let ppm = c.to_ppm();
        let mut lines = ppm.lines();

        // Skip header
        lines.next();
        lines.next();
        lines.next();

        assert_eq!(
            Some("255 204 153 255 204 153 255 204 153 255 204 153 255 204 153 255 204"),
            lines.next());
        assert_eq!(Some("153 255 204 153 255 204 153 255 204 153 255 204 153"), lines.next());
        assert_eq!(
            Some("255 204 153 255 204 153 255 204 153 255 204 153 255 204 153 255 204"),
            lines.next());
        assert_eq!(Some("153 255 204 153 255 204 153 255 204 153 255 204 153"), lines.next());
    }

    #[test]
    fn ppm_files_are_terminated_by_a_newline_character() {
        assert!(canvas(5, 3).to_ppm().ends_with("\n"));
    }
}
