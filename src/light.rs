use crate::tuple::Tuple;
use crate::color::Color;

#[derive(Debug, PartialEq)]
pub struct PointLight {
    pub position: Tuple,
    pub intensity: Color
}

pub fn point_light(position: Tuple, intensity: Color) -> PointLight {
    PointLight { position, intensity }
}

#[cfg(test)]
mod test {
    use crate::light::*;
    use crate::tuple::Tuple;
    use crate::color::Color;

    #[test]
    fn a_point_light_has_a_position_and_intensity() {
        let position = Tuple::ORIGIN;
        let intensity = Color::WHITE;
        let light = point_light(position, intensity);
        assert_eq!(light.position, position);
        assert_eq!(light.intensity, intensity);
    }
}
