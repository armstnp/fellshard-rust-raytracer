use crate::tuple::Tuple;
use crate::color::Color;
use crate::pattern::Pattern;
// TODO: Light trait
use crate::light::PointLight;
use crate::shape::Shape;

#[derive(Debug, Clone, PartialEq)]
pub struct Material {
    pub pattern: Pattern,
    pub ambient: f64,
    pub diffuse: f64,
    pub specular: f64,
    pub shininess: f64,
    pub reflective: f64,
    pub transparency: f64,
    pub refractive_index: f64,
    pub casts_shadows: bool
}

impl Material {
    pub fn lighting(&self, object: &dyn Shape, light: &PointLight, point: Tuple, eyev: Tuple, normalv: Tuple, in_shadow: bool) -> Color {
        let base_color = self.pattern.pattern_at_object(object, point);
        let effective_color = base_color * light.intensity;
        let ambient = effective_color * self.ambient;

        if in_shadow {
            return ambient;
        }

        let mut diffuse = Color::BLACK;
        let mut specular = Color::BLACK;

        let lightv = (light.position - point).norm();
        let light_dot_normal = lightv * normalv;

        if light_dot_normal >= 0. {
            diffuse = effective_color * self.diffuse * light_dot_normal;

            let reflectv = (-lightv).reflect(normalv);
            let reflect_dot_eye = reflectv * eyev;

            if reflect_dot_eye > 0. {
                let factor = reflect_dot_eye.powf(self.shininess);
                specular = light.intensity * self.specular * factor;
            }
        }

        ambient + diffuse + specular
    }
}

pub fn material() -> Material {
    use crate::pattern::solid;

    Material {
        pattern: solid(Color::WHITE),
        ambient: 0.1,
        diffuse: 0.9,
        specular: 0.9,
        shininess: 200.,
        reflective: 0.,
        transparency: 0.,
        refractive_index: 1.,
        casts_shadows: true
    }
}

#[cfg(test)]
mod test {
    use crate::material::*;
    use crate::tuple::{Tuple, point, vec};
    use crate::color::Color;
    use crate::light::point_light;
    use crate::pattern::solid;
    use crate::sphere::sphere;
    use approx::*;

    #[test]
    fn the_default_material() {
        let m = material();
        assert_eq!(m.pattern, solid(Color::WHITE));
        assert_eq!(m.ambient, 0.1);
        assert_eq!(m.diffuse, 0.9);
        assert_eq!(m.specular, 0.9);
        assert_eq!(m.shininess, 200.);
        assert_eq!(m.reflective, 0.);
        assert_eq!(m.transparency, 0.);
        assert_eq!(m.refractive_index, 1.);
        assert!(m.casts_shadows);
    }

    #[test]
    fn lighting_with_the_eye_between_the_light_and_the_surface() {
        let m = material();
        let position = Tuple::ORIGIN;
        let eyev = -Tuple::Z_UNITV;
        let normalv = -Tuple::Z_UNITV;
        let light = point_light(position - Tuple::Z_UNITV * 10, Color::WHITE);
        let result = m.lighting(&sphere(), &light, position, eyev, normalv, false);
        assert_eq!(result, Color::WHITE * 1.9);
    }

    #[test]
    fn lighting_with_the_eye_between_light_and_surface_with_eye_offset_45_deg() {
        let m = material();
        let position = Tuple::ORIGIN;
        let eyev = vec(0, 2f64.sqrt()/2., 2f64.sqrt()/-2.);
        let normalv = -Tuple::Z_UNITV;
        let light = point_light(position - Tuple::Z_UNITV * 10, Color::WHITE);
        let result = m.lighting(&sphere(), &light, position, eyev, normalv, false);
        assert_eq!(result, Color::WHITE);
    }

    #[test]
    fn lighting_with_eye_opposite_surface_with_light_offset_45_deg() {
        let m = material();
        let position = Tuple::ORIGIN;
        let eyev = -Tuple::Z_UNITV;
        let normalv = -Tuple::Z_UNITV;
        let light = point_light(point(0, 10, -10), Color::WHITE);
        let result = m.lighting(&sphere(), &light, position, eyev, normalv, false);
        assert_abs_diff_eq!(result, Color::WHITE * 0.7364, epsilon = 1e-4);
    }

    #[test]
    fn lighting_with_eye_in_the_path_of_the_reflection_vector() {
        let m = material();
        let position = Tuple::ORIGIN;
        let eyev = vec(0, 2f64.sqrt()/-2., 2f64.sqrt()/-2.);
        let normalv = -Tuple::Z_UNITV;
        let light = point_light(point(0, 10, -10), Color::WHITE);
        let result = m.lighting(&sphere(), &light, position, eyev, normalv, false);
        assert_abs_diff_eq!(result, Color::WHITE * 1.6364, epsilon = 1e-4);
    }

    #[test]
    fn lighting_with_the_light_behind_the_surface() {
        let m = material();
        let position = Tuple::ORIGIN;
        let eyev = -Tuple::Z_UNITV;
        let normalv = -Tuple::Z_UNITV;
        let light = point_light(position + Tuple::Z_UNITV * 10, Color::WHITE);
        let result = m.lighting(&sphere(), &light, position, eyev, normalv, false);
        assert_eq!(result, Color::WHITE * 0.1);
    }

    #[test]
    fn lighting_with_the_surface_in_shadow() {
        let m = material();
        let position = Tuple::ORIGIN;
        let eyev = -Tuple::Z_UNITV;
        let normalv = -Tuple::Z_UNITV;
        let light = point_light(Tuple::ORIGIN - Tuple::Z_UNITV * 10, Color::WHITE);
        let result = m.lighting(&sphere(), &light, position, eyev, normalv, true);
        assert_eq!(result, Color::WHITE * 0.1);
    }
}
